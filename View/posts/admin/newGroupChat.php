<?php require_once __DIR__ . "/../../basic/header.php" ?>

<a href="chat" class="btn btn-primary my-3">Alle Unterhaltungen</a>
<h4>Starte eine neue Gruppenunterhaltung</h4>


<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
   <div>
      <label for="groupChatUser1">Teilnehmer 1: </label>
      <select name="groupChatUser1" id="groupChatUser1">
         <option value="">User auswählen</option>
         <?php foreach ($allUsers as $user) : ?>
            <option value="<?php echo $user->username ?>" <?php if (!empty($_POST['groupChatUser1']) && $user->username == $_POST['groupChatUser1']) echo "selected=selected" ?>><?php echo $user->username ?> </option>
         <?php endforeach ?>
      </select>
   </div>
   <div>

      <div>
         <label for="groupChatUser2">Teilnehmer 2: </label>
         <select name="groupChatUser2" id="groupChatUser2">
            <option value="">User auswählen</option>
            <?php foreach ($allUsers as $user) : ?>
               <option value="<?php echo $user->username ?>" <?php if (!empty($_POST['groupChatUser1']) && $user->username == $_POST['groupChatUser1']) echo "selected=selected" ?>><?php echo $user->username ?> </option>
            <?php endforeach ?>
         </select>
         <div class="text-danger"><?php if ($errorSameUser != false) echo $errorSameUser ?></div>
      </div>
   </div>
   <div>
      <label for="msg">Nachricht hinterlassen</label>
   </div>
   <div>
      <textarea name="msg" id="msg" cols="50" rows="5" placeholder="Deine Nachricht"><?php if (!empty($_POST['msg'])) echo $_POST['msg'] ?></textarea>
   </div>
   <input type="hidden" name="_token" value="<?= $_SESSION['_token'] ?>">
   <button type="submit" class="btn btn-primary">Nachricht abschicken</button>
</form>

<?php require_once __DIR__ . "/../../basic/footer.php" ?>