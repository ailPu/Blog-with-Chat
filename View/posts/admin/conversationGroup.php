<?php require_once __DIR__ . "/../../basic/header.php" ?>

<a href="chat" class="btn btn-primary my-3">Alle Unterhaltungen</a>
<h4>Dein Chatverlauf mit <?php echo $chatPartner1 ?> und <?php echo $chatPartner2 ?></h4>

<form action="<?php echo $_SERVER['PHP_SELF'] . '?groupChatUser1=' . $chatPartner1 . '&groupChatUser2=' . $chatPartner2 ?>" method="post">
   <div>
      <label for="msg">Nachricht hinterlassen</label>
   </div>
   <div>
      <textarea name="msg" id="msg" cols="50" rows="5" placeholder="Deine Nachricht"><?php if (!empty($_POST['msg'])) echo $_POST['msg'] ?></textarea>
   </div>
   <input type="hidden" name="_token" value="<?= $_SESSION['_token'] ?>">
   <button type="submit" class="btn btn-primary">Nachricht abschicken</button>
</form>

<ul class="list-unstyled p-0 mt-1">
   <?php foreach ($chatGroupHistory as $message) : ?>
      <div class="card">
         <li class="card-body d-flex 
         <?php if ($_SESSION['username'] == $message->username) echo "bg-primary text-white" ?>
         <?php if ($chatPartner1 == $message->username) echo "bg-success text-white" ?>
         <?php if ($chatPartner2 == $message->username) echo "bg-danger text-white" ?>
         ">
            <div class="col-8 align-self-center">
               <?php echo nl2br($message->msg) ?>
            </div>
            <small class="align-self-center ml-auto col-4">geschrieben von <strong><?php if (in_array($message->username, $allUsersArray)) echo $message->username;
                                                                                    else echo "User gelöscht" ?></strong> am <?php echo $message->timestamp ?></small>
         </li>
      </div>
   <?php endforeach ?>
</ul>



<?php require_once __DIR__ . "/../../basic/footer.php" ?>