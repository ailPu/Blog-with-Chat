<?php require_once __DIR__ . "/../../basic/header.php" ?>
<?php
//  header("refresh:60"); 
?>

<h3 class="mt-4">Neue Unterhaltung starten</h3>

<a href="new1On1Chat">Neue 1on1 Unterhaltung starten</a>
<br>
<a href="newGroupChat">Neue Gruppen Unterhaltung starten</a>

<h4 class="mt-3">Deine 1On1 Unterhaltungen</h4>
<ul class="d-flex flex-column">

   <?php foreach ($all1On1ConversationsFromUser as $conversation) : ?>
      <?php if (in_array($conversation->chatPartnerID, $allUserIDs) && in_array($conversation->chatStarterID, $allUserIDs)) : ?>
         <?php if ($conversation->chatStarterID != $_SESSION['userID']) : ?>
            <?php $conversation->chatPartnerID = $conversation->chatStarterID ?>
            <?php $conversation->chatStarterID = $_SESSION['userID'] ?>
         <?php endif ?>
         <?php if (in_array($conversation->chatPartnerID, $blockedByUser)) : ?>
            <li class="d-flex align-items-center position-relative pl-1">
               <a href="conv1On1?chatID=<?php echo $conversation->id ?>">
                  Unterhaltung mit
                  <?php
                  echo $allChatPartnerNames[$conversation->chatPartnerID] . " <span class=\"text-warning\">(von dir beendet)</span></a>";
                  echo
                     "<div class=\"ml-auto mb-1\"><a href=\"unblockUserFromOverview?chatID=$conversation->id\" class=\"btn btn-success \">Unterhaltung wieder aufnehmen</a> </div>";
                  ?>
                  <?php if ($unreadMsgArray[$conversation->id] != 0) echo
                     "<div class=\"unreadMsg-chat position-absolute d-flex align-items-center p-0 justify-content-end\">
                        <span> {$unreadMsgArray[$conversation->id]} </span>
                        <i class=\"fas fa-envelope text-warning ml-1\"></i>
                        </div>" ?>
            </li>
         <?php elseif (in_array($conversation->chatPartnerID, $blockedByPartners)) : ?>
            <li class="d-flex align-items-center position-relative pl-1">
               <a href="conv1On1?chatID=<?php echo $conversation->id ?>">
                  Unterhaltung mit
                  <?php echo
                     $allChatPartnerNames[$conversation->chatPartnerID] . "<span class='text-danger'>  ({$allChatPartnerNames[$conversation->chatPartnerID]} hat dich blockiert)</span>";
                  ?></a>
               <?php if ($unreadMsgArray[$conversation->id] != 0) echo
                  "<div class=\"unreadMsg-chat position-absolute d-flex align-items-center p-0 justify-content-end\">
                     <span> {$unreadMsgArray[$conversation->id]} </span>
                        <i class=\"fas fa-envelope text-warning ml-1\"></i>
                     </div>" ?>
            </li>
         <?php else : ?>
            <li class="d-flex align-items-center position-relative pl-1">
               <a href="conv1On1?chatID=<?php echo $conversation->id ?>">
                  Unterhaltung mit
                  <?php echo
                     $allChatPartnerNames[$conversation->chatPartnerID];
                  ?></a>
               <?php if ($unreadMsgArray[$conversation->id] != 0) echo
                  "<div class=\"unreadMsg-chat position-absolute d-flex align-items-center p-0 justify-content-end\">
                        <span> {$unreadMsgArray[$conversation->id]} </span>
                        <i class=\"fas fa-envelope text-warning ml-1\"></i>
                        </div>" ?>
               <a class="btn btn-warning ml-1 ml-auto mb-1" href="blockUserFromOverview?chatID=<?php echo $conversation->id ?>">Unterhaltung beenden</a>
            </li>
         <?php endif ?>
      <?php endif ?>
   <?php endforeach ?>
</ul>


<h4 class="mt-3">Deine 1On1 Unterhaltungen mit gelöschten Usern</h4>
<ul class="d-flex flex-column-reverse">

   <?php foreach ($all1On1ConversationsFromUser as $conversation) : ?>
      <?php if (!in_array($conversation->chatPartnerID, $allUserIDs) || !in_array($conversation->chatStarterID, $allUserIDs)) : ?>
         <?php if ($conversation->chatStarterID != $_SESSION['userID']) : ?>
            <?php $conversation->chatPartnerID = $conversation->chatStarterID ?>
            <?php $conversation->chatStarterID = $_SESSION['userID'] ?>
         <?php endif ?>
         <li class="d-flex align-items-center position-relative pl-1">
            <a href="conv1On1?chatID=<?php echo $conversation->id ?>">
               Unterhaltung mit UserID:
               <?php
               echo $conversation->chatPartnerID ?>
            </a>
            <?php if ($unreadMsgArray[$conversation->id] != 0) echo
               "<div class=\"unreadMsg-chat position-absolute d-flex align-items-center p-0 justify-content-end\">
                     <span> {$unreadMsgArray[$conversation->id]} </span>
                        <i class=\"fas fa-envelope text-warning ml-1\"></i>
                     </div>" ?>
         </li>
      <?php endif ?>
   <?php endforeach ?>
</ul>


<!-- AUFGELASSEN -->

<h4 class="mt-4">Deine Gruppen Unterhaltungen</h4>

<ul>
   <?php
   $conversationGroupUsernames = [];

   $_SESSION['username'] = $_SESSION['username'];
   foreach ($convGroup as $conversation) : ?>
      <?php if (!in_array($conversation->username, $conversationGroupUsernames) || !in_array($conversation->groupChatUser1, $conversationGroupUsernames) || !in_array($conversation->groupChatUser2, $conversationGroupUsernames)) :

         $conversationGroupUsernames[] = $conversation->username;
         $conversationGroupUsernames[] = $conversation->groupChatUser1;
         $conversationGroupUsernames[] = $conversation->groupChatUser2;

      ?>
         <?php if ($conversation->username == $_SESSION['username']) : ?>
            <li>
               <a href="conversationsGroup?groupChatUser1=<?php echo $conversation->groupChatUser1 ?>&groupChatUser2=<?php echo $conversation->groupChatUser2 ?>">Unterhaltung mit <?php echo $conversation->groupChatUser1 . ' und ' . $conversation->groupChatUser2 ?></a>
            </li>

         <?php elseif ($conversation->groupChatUser1  == $_SESSION['username']) : ?>
            <li>
               <a href="conversationsGroup?groupChatUser1=<?php echo $conversation->username ?>&groupChatUser2=<?php echo $conversation->groupChatUser2 ?>">Unterhaltung mit <?php echo $conversation->username . ' und ' . $conversation->groupChatUser2 ?></a>
            </li>

         <?php elseif ($conversation->groupChatUser2  == $_SESSION['username']) : ?>
            <li>
               <a href="conversationsGroup?groupChatUser1=<?php echo $conversation->groupChatUser1 ?>&groupChatUser2=<?php echo $conversation->username ?>">Unterhaltung mit <?php echo $conversation->groupChatUser1 . ' und ' . $conversation->username ?></a>
            </li>
         <?php endif ?>
      <?php endif  ?>
   <?php endforeach ?>
</ul>

<?php require_once __DIR__ . "/../../basic/footer.php" ?>