<?php

namespace app\Login;

use app\Core\AbstractController;
use app\Login\UserAccountsRepository;

class LoginService extends AbstractController
{
   public function __construct(UserAccountsRepository $userAccountsRepository)
   {
      $this->userAccountsRepository = $userAccountsRepository;
   }

   public function loginAttempt($username, $password)
   {
      $user = $this->userAccountsRepository->findByUsername($username);
      if (empty($user)) {
         return false;
      }
      //! Für alte User, wo es noch kein Passwordhashing gab 
      if ($password === $user->password) {
         $_SESSION['username'] = $user->username;
         $_SESSION['userID'] = $user->id;
         session_regenerate_id();
         return true;
      }

      if (password_verify($password, $user->password)) {
         $_SESSION['username'] = $user->username;
         $_SESSION['userID'] = $user->id;
         session_regenerate_id();
         return true;
      } else {
         return false;
      }
   }

   public function logout()
   {
      //! löscht alle Daten aus $_SESSION. ACHTUNG NICHT unset($_SESSION) verwenden, da dann anscheinend der Login nicht mehr geht oder so...  
      session_unset();
      // session_regenerate_id();
      //! Nicht besser session_destroy?? --> angeblich hier und da nicht von Vorteil, wenn man Daten noch auswerten möchte. Denn session_destroy löscht die ganze sessions Datei. Vielleicht will man ja auslesen, wann sich User ausgeloggt hat oder sowas... Nicht ganz eindeutig ...
      //! Für komplette Datenentfernun session_unset() UND session_destroy 
      session_destroy();
      header("LOCATION: login");
   }
}
