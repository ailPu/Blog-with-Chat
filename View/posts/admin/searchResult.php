<?php require_once __DIR__ . "/../../basic/header.php" ?>

<div class="container">
   <div class="d-flex align-items-center">
      <?php if (!is_null($searchQuery)) : ?>
         <h3>Suchergebnisse für <?php echo $searchQuery ?></h3>
      <?php else : ?>
         <h3>Es wurde nichts ins Suchfeld eingegeben </h3>
      <?php endif ?>
      <form id="search" class="d-flex ml-auto bg-info rounded" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
         <div class="d-flex align-items-center">
            <label class="my-0 mr-2" for="search">Blogger suchen:</label>
            <input type="text" name="search" id="search">
         </div>
         <input type="hidden" name="_token" value="<?= $_SESSION['_token'] ?>">
         <button class="btn" type="submit">Suchen</button>
      </form>
   </div>

   <ul>
      <?php if ($found) : ?>
         <?php foreach ($foundUsers as $foundUser) : ?>
            <?php if ($foundUser->username == $_SESSION['username']) : continue ?>
            <?php endif ?>
            <li class="d-flex my-2 align-items-center">
               <a href="postsFrom?bloggerID=<?php echo $foundUser->id ?>">
                  <?php echo $foundUser->username ?>
               </a>
               <?php if (!in_array($foundUser->id, $allAboBloggerIDs)) : ?>
                  <a class=" btn btn-success ml-auto" href="subscribeFromSearch?bloggerID=<?php echo $foundUser->id ?>">Abonnieren</a>
               <?php else : ?>
                  <a class="btn btn-warning ml-auto" href="unsubscribeFromSearch?bloggerID=<?php echo $foundUser->id ?>">Abo stornieren</a>
               <?php endif ?>
            </li>
         <?php endforeach ?>
      <?php elseif (!$found && !is_null($found)) : ?>
         <p>Es wurde kein Nutzer namens "<?php echo $searchQuery ?>" gefunden </p>
      <?php endif ?>
   </ul>
</div>

<?php require_once __DIR__ . "/../../basic/footer.php" ?>