<?php

namespace app\Login;

use app\Core\AbstractModel;

class UserAccountsModel extends AbstractModel
{
   public $id;
   public $username;
   public $password;
   public $removeAccount;
}
