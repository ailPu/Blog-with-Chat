<?php

namespace app\Chat;

use app\Core\AbstractModel;

class ChatBlockedModel extends AbstractModel
{
   public $blockedUserID;
   public $userID;
   public $timestamp;
}
