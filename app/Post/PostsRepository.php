<?php

namespace app\Post;

use DateTime;

class PostsRepository
{
   public $pdo;

   public function __construct(\PDO $pdo)
   {
      $this->pdo = $pdo;
   }

   public function all()
   {
      $sql = "SELECT * from posts__ WHERE modifiedDate is NULL ORDER BY creationDate desc";
      $stmt = $this->pdo->query($sql);
      $posts = $stmt->fetchAll(\PDO::FETCH_CLASS, "app\\Post\\PostModel");

      $sql = "SELECT * from posts__ WHERE modifiedDate IS NOT NULL order by modifiedDate desc";
      $stmt = $this->pdo->query($sql);
      $modifiedPosts = $stmt->fetchAll(\PDO::FETCH_CLASS, "app\\Post\\PostModel");

      //! Ich möchte das posts array ordnen nach creationDate und modifiedDate. UND dann muss ich noch eine Variable mitgeben, ob das modifiedDate größer ist als sein creationDate, um es anzuzeigen

      if ($modifiedPosts) {
         foreach ($modifiedPosts as $modifiedPost) {
            $modifiedPost->oldCreationDate = $modifiedPost->creationDate;
            $modifiedPost->creationDate = $modifiedPost->modifiedDate;
         }
      }

      $mergedPosts = array_merge($modifiedPosts, $posts);
      function val_sort($array, $key)
      {
         //! In meinem array ist an jedem Key ein Array, deshalb muss ich mit $k => $v arbeiten
         //! Ich übergebe in ein neues Array den Wert aus dem Unterarray an der Stelle key (diesen habe ich als 2.Parameter beim Funktionsaufruf mitgegeben, in diesem Fall "creationDate")  
         foreach ($array as $k => $v) {
            // die();
            $b[] = strtotime($v[$key]);
         }
         //! Einfaches sortieren, MUSS MIT "a" sein! Sonst werden Werte nicht korrekt in Ursprungsarray geschrieben.
         //! Hier muss ich noch ein bisschen grübeln
         //!  Ich brauche arsort, weil ich absteigend sortieren will!
         arsort($b);
         foreach ($b as $k => $v) {
            $c[] = $array[$k];
         }
         return $c;
      }
      if (!empty($mergedPosts)) {
         $sorted = val_sort($mergedPosts, 'creationDate');
         return $sorted;
      }
   }

   public function find($id)
   {
      $sql = "SELECT * FROM posts__ WHERE id = :id";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":id" => $id,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Post\\PostModel');
      $post = $stmt->fetch(\PDO::FETCH_CLASS);
      return $post;
   }

   public function allPostsFromBlogger($creatorID)
   {
      $sql = "SELECT * FROM posts__ WHERE creatorID = :creatorID AND modifiedDate IS NULL ORDER BY creationDate desc";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":creatorID" => $creatorID,
      ]);
      $posts = $stmt->fetchall(\PDO::FETCH_CLASS, "app\\Post\\PostModel");

      $sql = "SELECT * FROM posts__ WHERE creatorID = :creatorID AND modifiedDate IS NOT NULL ORDER BY creationDate desc";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":creatorID" => $creatorID,
      ]);
      $modifiedPosts = $stmt->fetchall(\PDO::FETCH_CLASS, "app\\Post\\PostModel");

      if ($modifiedPosts) {
         foreach ($modifiedPosts as $modifiedPost) {
            $modifiedPost->oldCreationDate = $modifiedPost->creationDate;
            $modifiedPost->creationDate = $modifiedPost->modifiedDate;
         }
      }

      $mergedPosts = array_merge($modifiedPosts, $posts);
      function val_sort($array, $key)
      {
         //! In meinem array ist an jedem Key ein Array, deshalb muss ich mit $k => $v arbeiten
         //! Ich übergebe in ein neues Array den Wert aus dem Unterarray an der Stelle key (diesen habe ich als 2.Parameter beim Funktionsaufruf mitgegeben, in diesem Fall "creationDate")  
         foreach ($array as $k => $v) {
            // die();
            $b[] = strtotime($v[$key]);
         }
         //! Einfaches sortieren, MUSS MIT "a" sein! Sonst werden Werte nicht korrekt in Ursprungsarray geschrieben.
         //! Hier muss ich noch ein bisschen grübeln
         //!  Ich brauche arsort, weil ich absteigend sortieren will!
         arsort($b);
         foreach ($b as $k => $v) {
            $c[] = $array[$k];
         }
         return $c;
      }
      if (!empty($mergedPosts)) {
         $sorted = val_sort($mergedPosts, 'creationDate');
         return $sorted;
      }
   }

   public function countUnreadPostsFromAbos($allBloggerIDs, $userID)
   {
      $sql = "SELECT * from abos__read WHERE userID = :userID";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $allReadPosts = $stmt->fetchAll(\PDO::FETCH_CLASS, 'app\\Post\\PostModel');

      $unreadPostsArray = [];
      foreach ($allBloggerIDs as $bloggerID) {
         $sql = "SELECT COUNT(*) as count FROM posts__ WHERE creatorID = :creatorID and id not in (SELECT postID FROM abos__read WHERE userID = :userID)";
         $stmt = $this->pdo->prepare($sql);
         $stmt->execute([
            ":creatorID" => $bloggerID,
            ":userID" => $userID,
         ]);
         $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Post\\PostModel');
         $unreadPost = $stmt->fetch(\PDO::FETCH_CLASS);
         $unreadPostsArray[$bloggerID] = intval($unreadPost->count);

         $sql = "SELECT * FROM posts__ where creatorID = :creatorID and modifiedDate IS NOT NULL";
         $stmt = $this->pdo->prepare($sql);
         $stmt->execute([
            ":creatorID" => $bloggerID,
         ]);
         $modifiedPosts = $stmt->fetchAll(\PDO::FETCH_CLASS, 'app\\Post\\PostModel');
         $modifiedTimestamps = [];
         if ($modifiedPosts) {
            foreach ($modifiedPosts as $modifiedPost) {
               $modifiedTimestamps[$modifiedPost->id] = $modifiedPost->modifiedDate;
            }
            foreach ($allReadPosts as $readPost) {
               if (array_key_exists($readPost->postID, $modifiedTimestamps) && $readPost->timestamp < $modifiedTimestamps[$readPost->postID]) {
                  $unreadPostsArray[$bloggerID]++;
               }
            }
         }
      }
      return $unreadPostsArray;
   }

   public function findForEdit($userID, $id)
   {
      $sql = "SELECT * FROM posts__ WHERE creatorID = :creatorID AND id = :id";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":creatorID" => $userID,
         ":id" => $id,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Post\\PostModel');
      $post = $stmt->fetch(\PDO::FETCH_CLASS);
      return $post;
   }

   public function allPostsFromUser($userID)
   {
      $sql = "SELECT * FROM posts__ WHERE creatorID = :creatorID and modifiedDate IS NULL ORDER BY creationDate desc";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":creatorID" => $userID,
      ]);
      $posts = $stmt->fetchall(\PDO::FETCH_CLASS, "app\\Post\\PostModel");

      $sql = "SELECT * FROM posts__ WHERE creatorID = :creatorID and modifiedDate IS NOT NULL ORDER BY creationDate desc";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":creatorID" => $userID,
      ]);
      $modifiedPosts = $stmt->fetchall(\PDO::FETCH_CLASS, "app\\Post\\PostModel");
      if ($modifiedPosts) {
         foreach ($modifiedPosts as $modifiedPost) {
            $modifiedPost->oldCreationDate = $modifiedPost->creationDate;
            $modifiedPost->creationDate = $modifiedPost->modifiedDate;
         }
      }
      $mergedPosts = array_merge($modifiedPosts, $posts);
      function val_sort($array, $key)
      {
         //! In meinem array ist an jedem Key ein Array, deshalb muss ich mit $k => $v arbeiten
         //! Ich übergebe in ein neues Array den Wert aus dem Unterarray an der Stelle key (diesen habe ich als 2.Parameter beim Funktionsaufruf mitgegeben, in diesem Fall "creationDate")  
         foreach ($array as $k => $v) {
            $b[] = strtotime($v[$key]);
         }
         //! Einfaches sortieren, MUSS MIT "a" sein! Sonst werden Werte nicht korrekt in Ursprungsarray geschrieben.
         //! Hier muss ich noch ein bisschen grübeln
         //!  Ich brauche arsort, weil ich absteigend sortieren will!
         arsort($b);
         foreach ($b as $k => $v) {
            $c[] = $array[$k];
         }
         return $c;
      }
      if (!empty($mergedPosts)) {
         $sorted = val_sort($mergedPosts, 'creationDate');
         return $sorted;
      }
   }

   public function update($content, $title, $id, $timestamp)
   {
      $sql = "UPDATE posts__ SET title = :title, content = :content, modifiedDate = :modifiedDate WHERE id = :id";
      $stmt = $this->pdo->prepare($sql);
      if ($stmt->execute([
         ":title" => $title,
         ":content" => $content,
         ":id" => $id,
         ":modifiedDate" => $timestamp,
      ])) {
         echo "Post erfolgreich geändert";
      } else {
         echo "Fehler bei der Datenübertragung";
      }
   }

   public function deletePost($userID, $id)
   {
      $sql = "DELETE from posts__ WHERE creatorID = :creatorID AND id = :id";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":creatorID" => $userID,
         ":id" => $id
      ]);
   }

   public function deleteAllPosts()
   {
      $sql = "DELETE FROM posts__ WHERE creatorID not in (SELECT id from users__)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();
   }

   public function add($userID, $title, $content)
   {
      $sql = "INSERT INTO posts__ (creatorID, title, content) VALUES (:creatorID, :title, :content)";
      $stmt = $this->pdo->prepare($sql);
      if ($stmt->execute([
         ":creatorID" => $userID,
         ":title" => $title,
         ":content" => $content,
      ])) {
         echo "Dein Post wurde hinzugefügt";
      } else {
         echo "Fehler bei der Datenübertragung";
      }
   }

   public function setPostRead($postID, $userID)
   {
      $sql = "INSERT into abos__read (postID, userID) SELECT id,:userID FROM posts__ WHERE id = :id and id NOT in (SELECT postID from abos__read WHERE userID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":id" => $postID,
         ":userID" => $userID
      ]);

      $timestamp = new DateTime();
      $formattedTimestamp = $timestamp->format('c');
      $sql = "UPDATE abos__read SET timestamp = :timestamp WHERE ((SELECT creationDate from posts__ WHERE id = :postID) < (SELECT timestamp from abos__read WHERE postID = :postID AND userID = :userID)) and postID = :postID AND userID = :userID";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
         ":postID" => $postID,
         ":timestamp" => $formattedTimestamp,
      ]);
   }

   public function checkIfPostsUnread($postIDs, $userID)
   {
      $unreadPostsArray = [];
      foreach ($postIDs as $postID) {
         $sql = "SELECT id FROM posts__ WHERE id = :id and id not in (SELECT postID FROM abos__read WHERE userID = :userID)";
         $stmt = $this->pdo->prepare($sql);
         $stmt->execute([
            ":id" => $postID,
            ":userID" => $userID,
         ]);
         $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Post\\PostModel');
         $unreadPosts = $stmt->fetch(\PDO::FETCH_CLASS);
         if ($unreadPosts) {
            $unreadPostsArray[] = $unreadPosts->id;
         }
      }
      return $unreadPostsArray;
   }

   public function checkIfPostsModified($postIDs, $userID)
   {
      $modifiedPostsArray = [];
      foreach ($postIDs as $postID) {
         $sql = "SELECT id FROM posts__ WHERE id = :id AND modifiedDate > (SELECT timestamp from abos__read WHERE postID = :id AND userID = :userID)
         ";
         $stmt = $this->pdo->prepare($sql);
         $stmt->execute([
            ":id" => $postID,
            ":userID" => $userID,
         ]);
         $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Post\\PostModel');
         $modifiedPosts = $stmt->fetch(\PDO::FETCH_CLASS);
         if ($modifiedPosts) {
            $modifiedPostsArray[] = $modifiedPosts->id;
         }
      }
      return $modifiedPostsArray;
   }
}
