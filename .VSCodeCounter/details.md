# Details

Date : 2020-04-19 17:16:51

Directory d:\_Schnellzugriff Ordner\Dokumente\_Wifi\_eigenesFramework\app

Total : 31 files,  2837 codes, 154 comments, 405 blanks, all 3396 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [app/Abo/AboModel.php](/app/Abo/AboModel.php) | PHP | 10 | 0 | 4 | 14 |
| [app/Abo/AboRepository.php](/app/Abo/AboRepository.php) | PHP | 115 | 0 | 15 | 130 |
| [app/Chat/ChatBlockedModel.php](/app/Chat/ChatBlockedModel.php) | PHP | 9 | 0 | 4 | 13 |
| [app/Chat/ChatGroupModel.php](/app/Chat/ChatGroupModel.php) | PHP | 12 | 0 | 4 | 16 |
| [app/Chat/ChatMessagesModel.php](/app/Chat/ChatMessagesModel.php) | PHP | 11 | 0 | 4 | 15 |
| [app/Chat/ChatModel.php](/app/Chat/ChatModel.php) | PHP | 11 | 0 | 4 | 15 |
| [app/Chat/ChatRepository ohne Nachricht starten.php](/app/Chat/ChatRepository ohne Nachricht starten.php) | PHP | 122 | 0 | 15 | 137 |
| [app/Chat/ChatRepository.php](/app/Chat/ChatRepository.php) | PHP | 316 | 3 | 42 | 361 |
| [app/Chat/ChatRepository_.php](/app/Chat/ChatRepository_.php) | PHP | 122 | 0 | 15 | 137 |
| [app/Comment/CommentModel.php](/app/Comment/CommentModel.php) | PHP | 11 | 0 | 4 | 15 |
| [app/Comment/CommentsRepository.php](/app/Comment/CommentsRepository.php) | PHP | 186 | 4 | 22 | 212 |
| [app/Controller/DashboardController ohne Nachricht starten.php](/app/Controller/DashboardController ohne Nachricht starten.php) | PHP | 262 | 26 | 49 | 337 |
| [app/Controller/DashboardController.php](/app/Controller/DashboardController.php) | PHP | 528 | 25 | 47 | 600 |
| [app/Controller/DashboardController_.php](/app/Controller/DashboardController_.php) | PHP | 262 | 26 | 49 | 337 |
| [app/Controller/LoginController.php](/app/Controller/LoginController.php) | PHP | 76 | 4 | 16 | 96 |
| [app/Controller/PostsController.php](/app/Controller/PostsController.php) | PHP | 144 | 6 | 14 | 164 |
| [app/Core/AbstractController.php](/app/Core/AbstractController.php) | PHP | 10 | 0 | 3 | 13 |
| [app/Core/AbstractModel.php](/app/Core/AbstractModel.php) | PHP | 22 | 0 | 7 | 29 |
| [app/Core/Container.php](/app/Core/Container.php) | PHP | 90 | 0 | 7 | 97 |
| [app/Login/LoginService.php](/app/Login/LoginService.php) | PHP | 38 | 5 | 7 | 50 |
| [app/Login/UserAccountsModel.php](/app/Login/UserAccountsModel.php) | PHP | 10 | 0 | 4 | 14 |
| [app/Login/UserAccountsRepository.php](/app/Login/UserAccountsRepository.php) | PHP | 113 | 1 | 14 | 128 |
| [app/Login/UsernamesModel.php](/app/Login/UsernamesModel.php) | PHP | 8 | 0 | 4 | 12 |
| [app/Post/PostModel.php](/app/Post/PostModel.php) | PHP | 11 | 0 | 4 | 15 |
| [app/Post/PostsRepository.php](/app/Post/PostsRepository.php) | PHP | 277 | 18 | 29 | 324 |
| [app/Traits/AllUserIDsArray.php](/app/Traits/AllUserIDsArray.php) | PHP | 16 | 0 | 3 | 19 |
| [app/Traits/UnreadCounter.php](/app/Traits/UnreadCounter.php) | PHP | 14 | 1 | 5 | 20 |
| [app/functions/allConv1On1IDs.php](/app/functions/allConv1On1IDs.php) | PHP | 10 | 1 | 2 | 13 |
| [app/functions/allPostCreators.php](/app/functions/allPostCreators.php) | PHP | 10 | 0 | 2 | 12 |
| [app/functions/allUserIDs.php](/app/functions/allUserIDs.php) | PHP | 10 | 0 | 2 | 12 |
| [app/functions/isAuth.php](/app/functions/isAuth.php) | PHP | 1 | 34 | 4 | 39 |

[summary](results.md)