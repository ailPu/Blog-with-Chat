<?php require_once __DIR__ . "/../../basic/header.php" ?>

<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
   <h5>Email senden</h5>
   <div>
      <label for="subject">Betreff:</label>
   </div>
   <div>
      <input type="text" name="subject" id="subject" value="<?php if (!empty($_POST['subject'])) echo $_POST['subject'] ?>">
   </div><br>
   <div>
      <textarea name="msg" id="msg" cols="50" rows="5"><?php if (!empty($_POST['msg'])) echo $_POST['msg'] ?></textarea>
   </div>
   <input type="hidden" name="_token" value="<?= $_SESSION['_token'] ?>">
   <button type="submit" class="btn btn-primary">Email senden</button>
</form>

<?php require_once __DIR__ . "/../../basic/footer.php" ?>