<?php

namespace app\Controller;

use app\Abo\AboRepository;
use app\Chat\ChatRepository;
use app\Post\PostsRepository;
use app\Comment\CommentsRepository;
use app\Core\AbstractController;
use app\Login\LoginService;
use app\Login\UserAccountsRepository;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
use app\Traits\UnreadCounter;
use app\Traits\AllUserIDsArray;
use DateTime;

//! DOCUMENT_ROOT IST GENIAL, WEIL DANN KANN MAN IMMER NORMAL ZUM PFAD FAHREN!!! Keine relativen Pfad-Verirrungen!
require_once($_SERVER['DOCUMENT_ROOT'] . '/_eigenesFramework/app/functions/allUserIDs.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/_eigenesFramework/app/functions/allConv1On1IDs.php');

class DashboardController extends AbstractController
{
   public function __construct(PostsRepository $postsRepository, CommentsRepository $commentsRepository, UserAccountsRepository $userAccountsRepository, PHPMailer $phpMailer, Exception $phpMailerException, SMTP $phpMailerSMTP, ChatRepository $chatRepository, AboRepository $aboRepository)
   {
      isAuth();
      $this->postsRepository = $postsRepository;
      $this->phpMailer = $phpMailer;
      $this->phpMailerException = $phpMailerException;
      $this->phpMailerSMTP = $phpMailerSMTP;
      $this->userAccountsRepository = $userAccountsRepository;
      $this->chatRepository = $chatRepository;
      $this->commentsRepository = $commentsRepository;
      $this->aboRepository = $aboRepository;
   }


   public function dashboard()
   {
      $this->userObj = $this->userAccountsRepository->findByUsername($_SESSION['username']);
      UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
      $follower = $this->aboRepository->countFollowers($_SESSION['userID']);
      $allPostsFromUser = $this->postsRepository->allPostsFromUser($_SESSION['userID']);
      $unreadCmtsArray = [];
      if (!is_null($allPostsFromUser)) {
         $unreadCmtsArray = $this->commentsRepository->countUnreadCmtsForPost($allPostsFromUser, $_SESSION['userID']);
      }
      $removeAccount = false;
      if ($this->userObj->removeAccount == 1) {
         $removeAccount = true;
      }
      $this->render("/../../View/dashboard.php", [
         "allPostsFromUser" => $allPostsFromUser,
         "removeAccount" => $removeAccount,
         "unreadCmtsArray" => $unreadCmtsArray,
         "follower" => $follower
      ]);
   }

   public function subscribe()
   {
      $bloggerID = $_GET['bloggerID'];
      $bloggerObj = $this->userAccountsRepository->findUserByID($bloggerID);
      if ($bloggerObj) {
         $bloggerID = $bloggerObj->id;
         $this->aboRepository->subscribe($_SESSION['userID'], $bloggerID);
         if ($_SERVER['PATH_INFO'] == "/subscribeFromSearch") {
            header("LOCATION: searchResult?searchResult={$_SESSION['search']}");
         }
      } else {
         echo "Es gibt diesen User nicht";
      }
   }

   public function unsubscribe()
   {
      $bloggerID = $_GET['bloggerID'];
      $bloggerObj = $this->userAccountsRepository->findUserByID($bloggerID);
      if ($bloggerObj) {
         $bloggerID = $bloggerObj->id;
         $this->aboRepository->unsubscribe($_SESSION['userID'], $bloggerID);
         if ($_SERVER['PATH_INFO'] == "/unsubscribeFromSearch") {
            header("LOCATION: searchResult?searchResult={$_SESSION['search']}");
         } elseif ($_SERVER['PATH_INFO'] == "/unsubscribe") {
            header("LOCATION: myAbos");
         }
      } else {
         echo "Es gibt diesen User nicht";
      }
   }

   public function abos()
   {
      UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
      $unreadPostsArray = [];
      $allBloggernames = [];
      $allAbosFromUser = $this->aboRepository->allAbosFromUser($_SESSION['userID']);
      if (!empty($allAbosFromUser)) {
         foreach ($allAbosFromUser as $abo) {
            $allBloggerIDsFromAbos[] = $abo->bloggerID;
            $allBloggernames[$abo->bloggerID] = ($this->userAccountsRepository->findUserByID($abo->bloggerID))->username;
         }
         $unreadPostsArray = $this->postsRepository->countUnreadPostsFromAbos($allBloggerIDsFromAbos, $_SESSION['userID']);
      }
      //! Ohne isset würde er mir beim aufrufen von MyAbos ausspucken, dass es einen undefinierten INDEX gibt... aber weshalb?? Formularüberprüfung findet ja eh nur statt, wenn Bedingungen erfüllt sind...?!?! 
      if (isset($_POST['search'])) {
         if (!empty(trim($_POST['search'])) && $_POST['_token'] == $_SESSION['_token']) {
            $searchQuery = trim($_POST['search']);
            header("LOCATION: searchResult?searchResult={$searchQuery}");
         }
      }

      $_SESSION['_token'] = bin2hex(random_bytes(32));
      $this->render("/../../View/posts/admin/myAbos.php", [
         "allAbosFromUser" => $allAbosFromUser,
         "unreadPostsArray" => $unreadPostsArray,
         "allBloggernames" => $allBloggernames
      ]);
   }

   public function searchResult()
   {
      UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
      $searchQuery = NULL;
      $foundUsers = NULL;
      $found = NULL;
      if (!empty($_GET)) {
         $searchQuery = $_GET['searchResult'];
         $_SESSION['search'] = $searchQuery;
      }
      if (isset($_POST['search'])) {
         if (!empty(trim($_POST['search'])) && $_POST['_token'] == $_SESSION['_token']) {
            $searchQuery = trim($_POST['search']);
            $_SESSION['search'] = $searchQuery;
         }
      }
      $allAbosFromUser = $this->aboRepository->allAbosFromUser($_SESSION['userID']);
      $allAboBloggerIDs = [];
      foreach ($allAbosFromUser as $abo) {
         $allAboBloggerIDs[$abo->bloggerID] = $abo->bloggerID;
      }
      if (!is_null($searchQuery)) {
         $foundUsers = $this->userAccountsRepository->searchUsers($searchQuery);
         if ($foundUsers) {
            $found = true;
         } else {
            $found = false;
         }
      }
      $this->render("/../../View/posts/admin/searchResult.php", [
         "searchQuery" => $searchQuery,
         "found" => $found,
         "allAbosFromUser" => $allAbosFromUser,
         "foundUsers" => $foundUsers,
         "allAboBloggerIDs" => $allAboBloggerIDs
      ]);
   }

   public function editPost()
   {
      $postID = $_GET['postID'];
      $post = $this->postsRepository->findForEdit($_SESSION['userID'], $postID);
      if ($post) {
         UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
         if (!empty($_POST['content']) && !empty($_POST['title']) && $_POST['_token'] == $_SESSION['_token']) {
            $content = $_POST['content'];
            $title = $_POST['title'];
            $timestamp = new DateTime();
            $formattedTimestamp = $timestamp->format('c');
            $this->postsRepository->update($content, $title, $postID, $formattedTimestamp);
         }
         $allCommentsForPost = $this->commentsRepository->allForPost($postID);
         $commentCreatorObjs = $this->commentsRepository->findCommentCreators($postID);
         $allCommentCreatorNames = [];
         $allCommentCreatorIDs = [];
         foreach ($commentCreatorObjs as $commentCreator) {
            $allCommentCreatorNames[$commentCreator->id] = $commentCreator->username;
            $allCommentCreatorIDs[$commentCreator->id] = $commentCreator->id;
         };
         $post = $this->postsRepository->find($postID);
         $_SESSION['_token'] = bin2hex(random_bytes(32));
         $this->render("/../../View/posts/admin/editPost.php", [
            "post" => $post,
            "allCommentsForPost" => $allCommentsForPost,
            "commentCreatorObjs" => $commentCreatorObjs,
            "allCommentCreatorNames" => $allCommentCreatorNames,
            "allCommentCreatorIDs" => $allCommentCreatorIDs
         ]);
      } else {
         header("LOCATION: dashboard");
      }
   }

   public function deleteComment()
   {
      $postID = $_GET['postID'];
      $post = $this->postsRepository->findForEdit($_SESSION['userID'], $postID);
      if ($post) {
         $cmtID = $_GET['commentID'];
         $cmtCheck = $this->commentsRepository->checkCommentExistsForPost($cmtID, $_GET['postID']);
         if ($cmtCheck) {
            $this->commentsRepository->deleteForPost($cmtID);
            header("LOCATION: editPost?postID=$postID");
         } else {
            header("LOCATION: editPost?postID=$postID");
         }
      } else {
         header("LOCATION: dashboard");
      }
   }

   public function setCommentRead()
   {
      $postID = $_GET['postID'];
      $post = $this->postsRepository->findForEdit($_SESSION['userID'], $postID);
      if ($post) {
         $cmtID = $_GET['cmtID'];
         $cmtCheck = $this->commentsRepository->checkCommentExistsForPost($cmtID, $_GET['postID']);
         if ($cmtCheck) {
            $cmtRead = $this->commentsRepository->checkCmtRead($cmtID, $_SESSION['userID']);
            if (!$cmtRead) {
               $this->commentsRepository->setCmtRead($cmtID, $_SESSION['userID']);
               header("LOCATION: post?postID=$postID");
            } else {
               header("LOCATION: post?postID=$postID");
            }
         } else {
            header("LOCATION: post?postID=$postID");
         }
      } else {
         header("LOCATION: dashboard");
      }
   }

   public function deletePost()
   {
      $postID = $_GET['postID'];
      $post = $this->postsRepository->findForEdit($_SESSION['userID'], $postID);
      if ($post) {
         $this->commentsRepository->deleteAllForPost($postID);
         $this->postsRepository->deletePost($_SESSION['userID'], $postID);
         header("LOCATION: dashboard");
      } else {
         header("LOCATION: dashboard");
      }
   }

   public function addPost()
   {
      UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
      if (!empty($_POST['title']) && !empty($_POST['content']) && $_POST['_token'] == $_SESSION['_token']) {
         $title = $_POST['title'];
         $content = $_POST['content'];
         $this->postsRepository->add($_SESSION['userID'], $title, $content);
      }
      $_SESSION['_token'] = bin2hex(random_bytes(32));
      $this->render("/../../View/posts/admin/addPost.php", []);
   }

   public function ownPosts()
   {
      UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
      $allPostsFromUser = $this->postsRepository->allPostsFromUser($_SESSION['userID']);
      $unreadCmtsArray = [];
      if (!is_null($allPostsFromUser)) {
         $unreadCmtsArray = $this->commentsRepository->countUnreadCmtsForPost($allPostsFromUser, $_SESSION['userID']);
      }
      $this->render("/../../View/posts/admin/ownPosts.php", [
         "allPostsFromUser" => $allPostsFromUser,
         "unreadCmtsArray" => $unreadCmtsArray
      ]);
   }

   public function deleteAccount()
   {
      //! Kann man in die Session speichern und abfragen (Standardwert $_SESSION[RemoveAccount = 0], dann bei Button Löschen auf 1 und dann volley wegballern) 
      $this->userObj = $this->userAccountsRepository->findByUsername($_SESSION['username']);
      if ($this->userObj->removeAccount == 0) {
         $this->userAccountsRepository->prepareAccountRemoval($_SESSION['userID']);
         header("LOCATION: dashboard");
      } else {
         $this->userAccountsRepository->deleteUser($_SESSION['userID']);
         //! Eine bessere Reihenfolge wäre
         //! DeleteAllReadCmts  
         $this->commentsRepository->deleteAllCmtsFromOwnPosts($_SESSION['userID']);
         $this->commentsRepository->deleteAllReadCmts();
         $this->postsRepository->deleteAllPosts();
         $this->chatRepository->deleteChat();
         $this->chatRepository->deleteAllReadMsgs();
         $this->aboRepository->deleteAllAbos();
         $this->aboRepository->deleteAllReadAbos();
         header("LOCATION: logout");
      }
   }

   public function stopDeleteAccount()
   {
      $this->userAccountsRepository->stopAccountRemoval($_SESSION['userID']);
      header("LOCATION: dashboard");
   }

   public function sendEmail()
   {
      UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
      if (!empty($_POST['subject']) && !empty($_POST['msg']) && $_SESSION['_token'] == $_POST['_token']) {
         $subject = $_POST['subject'];
         $msg = $_POST['msg'];

         try {
            $mail = new PHPMailer(true);
            $mail->SMTPOptions = array(
               'ssl' => array(
                  'verify_peer' => false,
                  'verify_peer_name' => false,
                  'allow_self_signed' => true
               )
            );
            //Server settings
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'mail.gmx.net';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'paulbaur@gmx.at';                     // SMTP username
            $mail->Password   = '25052ailpu!';                               // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            //Recipients
            $mail->setFrom("paulbaur@gmx.at", $_SESSION['username']); // Name is optional
            $mail->addAddress("paulbaur@gmx.at");
            // $mail->addAddress('ellen@example.com');               
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

            // Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name     // Add a recipient

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML when true
            $mail->Subject = $subject;
            $mail->Body    = $msg;
            // Selbe Mail nur ohne HTML Tags
            $mail->AltBody    = strip_tags($msg);

            $mail->send();
         } catch (\Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
         }
      }
      $_SESSION['_token'] = bin2hex(random_bytes(32));
      $this->render("/../../View/posts/admin/sendEmail.php", []);
   }

   public function chat()
   {
      //! Man könnte hier theoretisch gleich ein array mit den vorhandenen und nicht vorhandenen userIds erstellen, damit man nicht für beide Fälle ALLE Nachrichten durchgehen muss  
      UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
      $all1On1ConversationsFromUser = $this->chatRepository->all1On1Conv($_SESSION['userID']);
      $blockedByUser = $this->chatRepository->blockedByUser($_SESSION['userID']);
      $blockedByPartners = $this->chatRepository->blockedByPartners($_SESSION['userID']);
      $allChatPartners = $this->chatRepository->allChatPartners($_SESSION['userID']);
      $allChatPartnerNames = [];
      foreach ($allChatPartners as $chatPartner) {
         $allChatPartnerNames[$chatPartner->id] = $chatPartner->username;
      }
      $unreadMsgArray = $this->chatRepository->countUnreadMessagesForConv($all1On1ConversationsFromUser, $_SESSION['userID']);
      $convGroup = $this->chatRepository->allGroupConv();
      $allUserIDsArray = AllUserIDsArray::allUserIDsArray($this->userAccountsRepository);
      $this->render("/../../View/posts/admin/chatOverview.php", [
         "all1On1ConversationsFromUser" => $all1On1ConversationsFromUser,
         "convGroup" => $convGroup,
         "allUserIDs" => $allUserIDsArray,
         "unreadMsgArray" => $unreadMsgArray,
         "allChatPartnerNames" => $allChatPartnerNames,
         "blockedByUser" => $blockedByUser,
         "blockedByPartners" => $blockedByPartners
      ]);
   }

   public function conv1On1()
   {
      $chatPartner = Null;
      $chatID = $_GET['chatID'];
      $chatIDObj = $this->chatRepository->getChatByChatID($chatID, $_SESSION['userID']);
      if ($chatIDObj) {
         $chatPartnerID = $chatIDObj->chatPartnerID;
         if ($chatIDObj->chatPartnerID == $_SESSION['userID']) {
            $chatPartnerID = $chatIDObj->chatStarterID;
         };
         $chatPartnerObj = $this->userAccountsRepository->findUserByID($chatPartnerID);
         if ($chatPartnerObj) {
            $chatPartner = $chatPartnerObj->username;
         }
         $blockedByUser = $this->chatRepository->blockedByUser($_SESSION['userID']);
         $blockedByPartner = $this->chatRepository->blockedByPartner($_SESSION['userID'], $chatPartnerID);
         $allUserIDsArray = AllUserIDsArray::allUserIDsArray($this->userAccountsRepository);
         if (!empty($_POST['msg']) && $_SESSION['_token'] == $_POST['_token'] && !$blockedByPartner && !in_array($chatPartnerID, $blockedByUser)) {
            $msg = $_POST['msg'];
            $msg = htmlentities($msg, ENT_QUOTES, "utf-8");
            $msg = strip_tags($msg);
            $this->chatRepository->add1On1Msg($chatID, $_SESSION['userID'], $msg);
         }

         $chatHistory = $this->chatRepository->find1On1Conv($chatID);
         $this->chatRepository->setMsgRead($chatID, $_SESSION['userID']);
         UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
         $_SESSION['_token'] = bin2hex(random_bytes(32));
         $this->render("/../../View/posts/admin/conversation1On1.php", [
            "chatPartner" => $chatPartner,
            "chatPartnerID" => $chatPartnerID,
            "chatHistory" => $chatHistory,
            "chatIDObj" => $chatIDObj,
            "allUserIDs" => $allUserIDsArray,
            "chatID" => $chatID,
            "blockedByUser" => $blockedByUser,
            "blockedByPartner" => $blockedByPartner
         ]);
      } else {
         header("LOCATION: chat");
      }
   }


   public function new1On1Conv()
   {
      UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
      $allUsersExceptLoggedUser = $this->userAccountsRepository->allUsersExceptLoggedUser($_SESSION['username']);
      $allUserIDsExceptLoggedUserID = [];
      foreach ($allUsersExceptLoggedUser as $user) {
         $allUserIDsExceptLoggedUserID[] = $user->id;
      }
      $allChatPartners = $this->chatRepository->allChatPartners($_SESSION['userID']);
      $allChatPartnerIDs = [];
      foreach ($allChatPartners as $chatPartner) {
         $allChatPartnerIDs[] = $chatPartner->id;
      }
      $chatsWithEveryUser = false;
      if (!empty($allChatPartnerIDs)) {
         $chatsWithEveryUser = (array_diff($allUserIDsExceptLoggedUserID, $allChatPartnerIDs));
         if (empty($chatsWithEveryUser)) {
            $chatsWithEveryUser = true;
         } else {
            $chatsWithEveryUser = false;
         }
      }
      if (!empty($_POST['recipientUsername']) && $_SESSION['_token'] == $_POST['_token']) {
         $chatPartner = $_POST['recipientUsername'];
         $chatPartnerObj = $this->userAccountsRepository->findByUsername($chatPartner);
         $chatPartnerID = $chatPartnerObj->id;
         $allChatUsersObj = $this->chatRepository->all1On1Conv($_SESSION['userID']);
         $allChatUserIDs = [];
         foreach ($allChatUsersObj as $chatUser) {
            if (!in_array($chatUser->chatPartnerID, $allChatUserIDs)) {
               $allChatUserIDs[] = $chatUser->chatPartnerID;
            }
            if (!in_array($chatUser->chatStarterID, $allChatUserIDs)) {
               $allChatUserIDs[] = $chatUser->chatStarterID;
            }
         }
         if (!in_array($chatPartnerID, $allChatUserIDs)) {
            $this->chatRepository->add1On1Conv($_SESSION['userID'], $chatPartnerID);
            $chatIDObj = $this->chatRepository->getChatID($_SESSION['userID'], $chatPartnerID);
            $chatID = $chatIDObj->id;
            header("LOCATION: conv1On1?chatID=$chatID");
         } else {
            $chatIDObj = $this->chatRepository->getChatID($_SESSION['userID'], $chatPartnerID);
            $chatID = $chatIDObj->id;
            header("LOCATION: conv1On1?chatID=$chatID");
         }
      }
      $_SESSION['_token'] = bin2hex(random_bytes(32));
      $this->render("/../../View/posts/admin/new1On1Chat.php", [
         "allUsersExceptLoggedUser" => $allUsersExceptLoggedUser,
         "chatsWithEveryUser" => $chatsWithEveryUser,
         "allChatPartnerIDs" => $allChatPartnerIDs
      ]);
   }

   public function blockUser()
   {
      //! Hier muss noch die Löschfunktion für den chatoverview eingebaut werden 
      $chatID = $_GET['chatID']; {
         $chatObj = $this->chatRepository->getChatByChatID($chatID, $_SESSION['userID']);
         if (!$chatObj) {
            header("LOCATION: chat");
         } else {
            $chatPartnerID = $chatObj->chatPartnerID;
            if ($chatObj->chatPartnerID == $_SESSION['userID']) {
               $chatPartnerID = $chatObj->chatStarterID;
            };
            $blockedByUser = $this->chatRepository->blockedByUser($_SESSION['userID']);
            $blockedByPartner = $this->chatRepository->blockedByPartner($_SESSION['userID'], $chatPartnerID);
            if (!in_array($chatPartnerID, $blockedByUser) && !$blockedByPartner) {
               $this->chatRepository->blockUser($chatPartnerID, $_SESSION['userID']);
            }
         }
         if ($_SERVER['PATH_INFO'] == "/blockUserFromOverview") {
            header("LOCATION: chat");
         } elseif ($_SERVER['PATH_INFO'] == "/blockUser") {
            header("LOCATION: conv1On1?chatID=$chatID");
         }
      }
   }

   public function unblockUser()
   {
      $chatID = $_GET['chatID'];
      $chatObj = $this->chatRepository->getChatByChatID($chatID, $_SESSION['userID']);
      if (!$chatObj) {
         header("LOCATION: chat");
      } else {
         $chatPartnerID = $chatObj->chatPartnerID;
         if ($chatObj->chatPartnerID == $_SESSION['userID']) {
            $chatPartnerID = $chatObj->chatStarterID;
         };
         $blockedByUser = $this->chatRepository->blockedByUser($_SESSION['userID']);
         $blockedByPartner = $this->chatRepository->blockedByPartner($_SESSION['userID'], $chatPartnerID);
         if (in_array($chatPartnerID, $blockedByUser) && !$blockedByPartner) {
            $this->chatRepository->unblockUser($chatPartnerID, $_SESSION['userID']);
         }
         if ($_SERVER['PATH_INFO'] == "/unblockUserFromOverview") {
            header("LOCATION: chat");
         } elseif ($_SERVER['PATH_INFO'] == "/unblockUser") {
            header("LOCATION: conv1On1?chatID=$chatID");
         }
      }
   }


   // --------------------------------------------Features vorläufig aufgelassen----------------------------------------------------//



   //? Feature aufgelassen, da vorerst unnötig 

   //! Mit 3 Leuten Chat öffnen
   public function newGroupConversation()
   {
      $errorSameUser = false;

      $username = $_SESSION['username'];
      $allUsers = $this->userAccountsRepository->allUsersExceptLoggedUser($username);
      //! Bedingungen aufbrechen, zwecks Übersichtlichkeit 

      //! Formular 
      if (!empty($_POST['groupChatUser1']) && !empty($_POST['groupChatUser2'])) {
         if ($_POST['groupChatUser1'] == $_POST['groupChatUser2']) {
            $errorSameUser = "Teilnehmer 1 und Teilnehmer 2 können nicht gleich sein";
         } else if (!empty($_POST['msg']) && $_SESSION['_token'] == $_POST['_token']) {
            $msg = $_POST['msg'];
            $username = $_SESSION['username'];
            $chatPartner1 = $_POST['groupChatUser1'];
            $chatPartner2 = $_POST['groupChatUser2'];
            $this->chatRepository->addGroupConv($username, $chatPartner1, $chatPartner2, $msg);
            header("LOCATION: conversationsGroup?groupChatUser1=$chatPartner1&groupChatUser2=$chatPartner2");
         }
      }

      $_SESSION['_token'] = bin2hex(random_bytes(32));

      $this->render("/../../View/posts/admin/newGroupChat.php", [
         "allUsers" => $allUsers,
         "errorSameUser" => $errorSameUser,
      ]);
   }

   public function conversationsGroup()
   {
      //! Hole Namen der Unterhaltungspartners 
      $username = $_SESSION['username'];
      $chatPartner1 = $_GET['groupChatUser1'];
      $chatPartner2 = $_GET['groupChatUser2'];
      $allUsersObj = $this->userAccountsRepository->allUsers();
      $allUsersArray = [];

      foreach ($allUsersObj as $user) {
         if (!in_array($user->username, $allUsersArray)) {
            $allUsersArray[] = $user->username;
         }
      }

      if (!empty($_POST['msg']) && $_SESSION['_token'] == $_POST['_token']) {
         $msg = $_POST['msg'];
         $this->chatRepository->addGroupConv($username, $chatPartner1, $chatPartner2, $msg);
      }

      $chatGroupHistory = $this->chatRepository->findGroupConv($username, $chatPartner1, $chatPartner2);
      $_SESSION['_token'] = bin2hex(random_bytes(32));
      $this->render("/../../View/posts/admin/conversationGroup.php", [
         "chatPartner1" => $chatPartner1,
         "chatPartner2" => $chatPartner2,
         "chatGroupHistory" => $chatGroupHistory,
         "allUsersArray" => $allUsersArray
      ]);
   }
}
