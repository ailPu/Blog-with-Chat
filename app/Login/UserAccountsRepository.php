<?php

namespace app\Login;

class UserAccountsRepository
{
   public $pdo;

   public function __construct(\PDO $pdo)
   {
      $this->pdo = $pdo;
   }

   public function findByUsername($username)
   {
      $stmt = $this->pdo->prepare("SELECT * FROM users__ WHERE username = :username");
      $stmt->execute([
         ":username" => $username,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, "\\app\\Login\\UserAccountsModel");
      $user = $stmt->fetch(\PDO::FETCH_CLASS);
      return $user;
   }

   public function findUserByID($id)
   {
      $stmt = $this->pdo->prepare("SELECT id, username FROM users__ WHERE id = :id");
      $stmt->execute([
         ":id" => $id,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, "\\app\\Login\\UserAccountsModel");
      $user = $stmt->fetch(\PDO::FETCH_CLASS);
      return $user;
   }

   public function searchUsers($searchQuery)
   {
      $stmt = $this->pdo->prepare("SELECT id,username FROM users__ WHERE username LIKE '%$searchQuery%'");
      $stmt->execute([
         ":username" => $searchQuery,
      ]);
      $users = $stmt->fetchall(\PDO::FETCH_CLASS, "\\app\\Login\\UserAccountsModel");
      return $users;
   }

   public function addUser($username, $password)
   {
      //! Wenn User noch nicht vorhanden, dann füge ihn der Datenbank hinzu 
      if (!$this->checkIfUsernameTaken($username)) {
         $sql = "INSERT INTO users__ (username, password) VALUES (:username, :password)";
         $stmt = $this->pdo->prepare($sql);
         $stmt->execute([
            ":username" => $username,
            ":password" => $password,
         ]);
         $sql = "INSERT INTO usernames (username) VALUES (:username)";
         $stmt = $this->pdo->prepare($sql);
         $stmt->execute([
            ":username" => $username,
         ]);
         return true;
      } else {
         return false;
      }
   }

   public function checkIfUsernameTaken($username)
   {
      $sql = "SELECT username FROM users__ WHERE username = :username";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":username" => $username,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, "\\app\\Login\\UsernamesModel");
      $user = $stmt->fetch(\PDO::FETCH_CLASS);
      return $user;
   }

   public function deleteUser($userID)
   {
      $sql = "DELETE FROM users__ WHERE id = :id";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":id" => $userID
      ]);
   }

   public function allUsersExceptLoggedUser($username)
   {
      $sql = "SELECT id,username from users__ WHERE username <> :username ORDER BY username asc";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":username" => $username
      ]);;
      $allUsers = $stmt->fetchAll(\PDO::FETCH_CLASS, "\\app\\Login\\UserAccountsModel");
      return $allUsers;
   }

   public function allUsers()
   {
      $sql = "SELECT * from users__";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();
      $allUsers = $stmt->fetchAll(\PDO::FETCH_CLASS, "\\app\\Login\\UserAccountsModel");
      return $allUsers;
   }

   public function prepareAccountRemoval($userID, $remove = 1)
   {
      $sql = "UPDATE users__ SET removeAccount = :removeAccount WHERE id = :id";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":removeAccount" => $remove,
         ":id" => $userID
      ]);
   }

   public function stopAccountRemoval($userID, $remove = 0)
   {
      $sql = "UPDATE users__ SET removeAccount = :removeAccount WHERE id = :id";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":removeAccount" => $remove,
         ":id" => $userID
      ]);
   }
}
