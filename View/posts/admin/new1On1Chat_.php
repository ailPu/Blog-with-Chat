<?php require_once __DIR__ . "/../../basic/header.php" ?>

<a href="chat" class="btn btn-primary my-3">Alle Unterhaltungen</a>
<h4>Starte eine neue Unterhaltung</h4>


<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
   <div>
      <label for="recipientUsername"></label>
   </div>
   <div class="form-group">
      <select name="recipientUsername" id="recipientUsername">
         <option value="">User auswählen</option>
         <?php foreach ($allUsers as $user) : ?>
            <option value="<?php echo $user->username ?>"><?php echo $user->username ?> </option>
         <?php endforeach ?>
      </select>
   </div>
   <input type="hidden" name="_token" value="<?= $_SESSION['_token'] ?>">
   <button type="submit" class="btn btn-primary">Chat starten</button>
</form>

<?php require_once __DIR__ . "/../../basic/footer.php" ?>