<?php

namespace app\Traits;

trait AllUserIDsArray
{
   public static function allUserIDsArray($userAccountsRepository)
   {
      $allUsersObj = $userAccountsRepository->allUsers();
      $allUserIDsArray = [];
      foreach ($allUsersObj as $user) {
         if (!in_array($user->id, $allUserIDsArray)) {
            $allUserIDsArray[] = $user->id;
         }
      }
      return $allUserIDsArray;
   }
}
