<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <!-- Für meine CSS Datei habe ich jetzt sozusagen den Pfad ausgehend von meinem locahost (_Wifi) ausgehend verlinkt -->
   <link rel="stylesheet" type="text/css" href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/_eigenesFramework/style/style.css">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
   <title>Blog'n'Chat</title>
</head>

<body>
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

         <?php if (!isAuth()) : ?>
            <ul class="navbar-nav mr-auto">
               <li class="nav-item <?php if ($_SERVER['PATH_INFO'] == '/home') echo "active" ?>">
                  <a class="nav-link" href="home">Alle Posts<span class="sr-only"></span></a>
               </li>
               <li class="nav-item <?php if ($_SERVER['PATH_INFO'] == '/login') echo "active" ?>">
                  <a class="nav-link" href="login">Login <span class="sr-only"></span></a>
               </li>
            </ul>
         <?php endif ?>

         <?php if (isAuth()) : ?>
            <ul class="navbar-nav mr-auto">
               <li class="nav-item <?php if ($_SERVER['PATH_INFO'] == '/dashboard') echo "active" ?>">
                  <a class="nav-link" href="dashboard">Mein Dashboard <span class="sr-only"></span></a>
               </li>
               <li class="nav-item <?php if ($_SERVER['PATH_INFO'] == '/chat') echo "active" ?> position-relative">
                  <a class="nav-link" href="chat">Chat <span class="sr-only"></span></a>
                  <?php if ($_SESSION['unreadMsgCounterNav'] != 0) echo
                     "<div class=\"position-absolute unreadMsg\">
                        <span>" . $_SESSION['unreadMsgCounterNav'] . "</span>
                        <i class=\"fas fa-envelope text-warning\"></i>
                        </div>";
                  ?>
               </li>
               <li class="nav-item position-relative <?php if ($_SERVER['PATH_INFO'] == '/ownPosts') echo "active" ?>">
                  <a class="nav-link" href="ownPosts">Meine Posts <span class="sr-only"></span></a>
                  <?php if ($_SESSION['unreadCmtCounterNav'] != 0) echo
                     "<div class=\"position-absolute unreadCmt\">
                        <span>" . $_SESSION['unreadCmtCounterNav'] . "</span>
                        <i class=\"fas fa-envelope text-warning\"></i>
                     </div>" ?>
               </li>
               <li class="nav-item position-relative <?php if ($_SERVER['PATH_INFO'] == '/myAbos') echo "active" ?>">
                  <a class="nav-link" href="myAbos">Meine Abos <span class="sr-only"></span></a>
                  <?php if ($_SESSION['unreadAboCounterNav'] != 0) echo
                     "<div class=\"position-absolute unreadPost\">
                        <span>" . $_SESSION['unreadAboCounterNav'] . "</span>
                        <i class=\"fas fa-envelope text-warning\"></i>
                     </div>" ?>
               </li>
               <li class="nav-item <?php if ($_SERVER['PATH_INFO'] == '/home') echo "active" ?>">
                  <a class="nav-link" href="home">Alle Posts <span class="sr-only"></span></a>
               </li>
               <?php if ($_SERVER['PATH_INFO'] == '/dashboard') : ?>
                  <li class="nav-item">
                     <a class="nav-link" href="deleteAccount">Mein Konto löschen <span class="sr-only"></span></a>
                  </li>
               <?php endif ?>
               <li class="nav-item">
                  <a class="nav-link" href="logout">Logout <span class="sr-only"></span></a>
               </li>
            </ul>
         <?php endif ?>
         <span class="ml-auto text-primary"><?php if (!empty($_SESSION['username'])) echo "Eingeloggt als " . $_SESSION['username'] ?></span>
   </nav>

   <div class="container">