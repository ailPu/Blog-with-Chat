<?php

namespace app\Post;

use app\Core\AbstractModel;

class PostModel extends AbstractModel
{
   public $id;
   public $creatorID;
   public $title;
   public $content;
   public $creationDate;
}
