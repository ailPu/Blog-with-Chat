<?php

function allPostCreators($posts, $postCreatorsArray)
{
   foreach ($posts as $post) {
      if (!in_array($post->user, $postCreatorsArray)) {
         $postCreatorsArray[] = $post->user;
      }
   }
   return $postCreatorsArray;
}
