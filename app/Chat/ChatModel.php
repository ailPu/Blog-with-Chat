<?php

namespace app\Chat;

use app\Core\AbstractModel;

class ChatModel extends AbstractModel
{
   public $id;
   public $chatStarterID;
   public $chatPartnerID;
   public $blockForUser;
   public $timestamp;
}
