<?php

namespace app\Controller;

use app\Abo\AboRepository;
use app\Chat\ChatRepository;
use app\Post\PostsRepository;
use app\Core\AbstractController;
use app\Comment\CommentsRepository;
use app\Login\UserAccountsRepository;
use GUMP;
use app\Traits\UnreadCounter;
use EmptyIterator;

class PostsController extends AbstractController
{
   public function __construct(PostsRepository $postsRepository, GUMP $gump, CommentsRepository $commentsRepository, ChatRepository $chatRepository, AboRepository $aboRepository, UserAccountsRepository $userAccountsRepository)
   {
      $this->postsRepository = $postsRepository;
      $this->gump = $gump;
      $this->commentsRepository = $commentsRepository;
      $this->chatRepository = $chatRepository;
      $this->aboRepository = $aboRepository;
      $this->userAccountsRepository = $userAccountsRepository;
   }

   public function index()
   {
      $allPosts = $this->postsRepository->all();
      if (!empty($_SESSION['username'])) {
         UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
      }
      $this->render(
         "/../../View/posts/index.php",
         [
            "allPosts" => $allPosts,
         ]
      );
   }

   public function show()
   {
      // //! Braucht es das hier überhaupt?!? 
      if (empty($_GET['postID'])) {
         header("LOCATION: home");
      } else {
         $postID = $_GET['postID'];
         $postObj = $this->postsRepository->find($postID);
         if ($postObj) {
            $postCreatorObj = $this->userAccountsRepository->findUserByID($postObj->creatorID);
            $unreadCmtsArray = NULL;
            if (!empty($_POST['comment']) && $_SESSION['_token'] == $_POST['_token']) {
               $comment = $_POST['comment'];
               $this->gump->validation_rules([
                  'comment'    => 'max_len,1000|min_len,6',
               ]);

               $this->gump->filter_rules([
                  'comment' => 'trim|sanitize_string|strip_tags|htmlentities,ENT_QUOTES',
               ]);

               $valid_data = $this->gump->run($_POST);
               if ($valid_data) {
                  if (empty($_SESSION['username'])) {
                     $this->commentsRepository->insertForPost($comment, $postID);
                  } else {
                     if ($_SESSION['userID'] == $postCreatorObj->id)
                        $this->commentsRepository->insertForPost($comment, $postID, $_SESSION['userID'], $postCreatorObj);
                     else {
                        $this->commentsRepository->insertForPost($comment, $postID, $_SESSION['userID']);
                     }
                  }
               }
            }
            $gumpErrors = $this->gump->get_errors_array();

            //!findCommentCreators findet alle bis auf den postErsteller, denn den wollen wir ja anders ausgeben farblich, also evtl finCommentCreatorsEXCEPTPostCreator.... 
            $commentCreatorObjs = $this->commentsRepository->findCommentCreators($postID);
            $allCommentCreatorIDs = [];
            $allCommentCreatorNames = [];
            foreach ($commentCreatorObjs as $commentCreator) {
               $allCommentCreatorIDs[$commentCreator->id] = $commentCreator->id;
               $allCommentCreatorNames[$commentCreator->id] = $commentCreator->username;
            }

            $comments = $this->commentsRepository->allForPost($postID);

            //! Das gehört eigentlich irgendwie in einen eigenen PostsAdminController... Denn das Kommentare zählen ist unnötig für einen allgemeinen PostsController bzw. für alle, die nicht der PostCreator sind, aber dann irgendwo auch wieder blöd, weil man könnte ja auch als Eingeloggter User auf "Alle Posts" gehen. Da müsste man dann wahrscheinlich doch wieder irgendwie mit Cookies oder so arbeiten. Quasi, wo man herkommt oder so. Dann könnte man den PostsAdminController aufrufen ... allerdings würde man dann den Code hier erst recht doppelt schreiben. Neineineineine..... 
            if (!empty($_SESSION['username'])) {
               if ($_SESSION["userID"] == $postCreatorObj->id) {
                  $unreadCmts = $this->commentsRepository->unreadComments($postID, $_SESSION['userID']);
                  $unreadCmtsArray = [];
                  foreach ($unreadCmts as $unreadCmt) {
                     if (!in_array($unreadCmt->id, $unreadCmts)) {
                        $unreadCmtsArray[] = $unreadCmt->id;
                     }
                  }
               }
               if ($_SESSION['userID'] != $postCreatorObj->id) {
                  $this->postsRepository->setPostRead($postID, $_SESSION['userID']);
               }
               UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
            }
            //! ^^ Das gehört eigentlich in einen eigenen PostsAdminController ^^
            $_SESSION['_token'] = bin2hex(random_bytes(32));
            $this->render(
               "/../../View/posts/show.php",
               [
                  'postObj' => $postObj,
                  'comments' => $comments,
                  'gumpErrors' => $gumpErrors,
                  "postCreatorObj" => $postCreatorObj,
                  "commentCreatorObjs" => $commentCreatorObjs,
                  "unreadCmts" => $unreadCmtsArray,
                  "allCommentCreatorIDs" => $allCommentCreatorIDs,
                  "allCommentCreatorNames" => $allCommentCreatorNames
               ]
            );
         } else {
            header("LOCATION: home");
         }
      }
   }


   public function allPostsFromBlogger()
   {
      $bloggerID = $_GET['bloggerID'];
      $bloggerObj = $this->userAccountsRepository->findUserByID($bloggerID);
      if ($bloggerObj) {
         $allPostsFromBlogger = $this->postsRepository->allPostsFromBlogger($bloggerID);
         $bloggername = $bloggerObj->username;
         $follower = $this->aboRepository->countFollowers($bloggerID);
      } else {
         echo "Diesen Blogger gibt es nicht";
         header("LOCATION: home");
      };

      $allPostIDsFromBlogger = [];
      if ($allPostsFromBlogger) {
         foreach ($allPostsFromBlogger as $post) {
            $allPostIDsFromBlogger[] = $post->id;
         }
      }
      //! Auch das gehört eigentlich in einen PostsAdminController, denn Gäste können sich irgendwann vielleicht auch alle Posts von einem Blogger anschauen...
      $unreadPostIDsArray = [];
      if (!empty($_SESSION['username']) && $_SESSION['userID'] != $bloggerID) {
         $unreadPostIDsArray = $this->postsRepository->checkIfPostsUnread($allPostIDsFromBlogger, $_SESSION['userID']);
         $modifiedPostIDsArray = $this->postsRepository->checkIfPostsModified($allPostIDsFromBlogger, $_SESSION['userID']);
      }
      //! Das kann man eigentlich ins obere if schmeißen oder? 
      if (!empty($_SESSION['username'])) {
         UnreadCounter::unreadCounter($this->chatRepository, $this->commentsRepository, $this->aboRepository, $this->postsRepository, $_SESSION['userID']);
      }
      $this->render("/../../View/posts/allPostsFromBlogger.php", [
         "allPostsFromBlogger" => $allPostsFromBlogger,
         "unreadPostIDsArray" => $unreadPostIDsArray,
         "bloggername" => $bloggername,
         "modifiedPostIDsArray" => $modifiedPostIDsArray,
         "follower" => $follower
      ]);
   }
}
