<?php

use app\Core\Container;

require_once __DIR__ . "/vendor/autoload.php";


//! Evlt return false unten dazu geben, für nicht eingeloggte USer auf Home Seite 
function isAuth()
{
   if (!empty($_SESSION['username'])) {
      return true;
   } elseif (empty($_SESSION['username'])) {
      switch ($_SERVER['PATH_INFO']) {
         case "/login":
            break;
         case "/home":
            break;
         case "/post":
            break;
         default:
            header("LOCATION: login");
            break;
      }
   }
}
//Evtl Funktionen zum Überprüfen von Einträgen auf Sicherheit
$container = new Container;
