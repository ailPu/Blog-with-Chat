<?php require_once __DIR__ . "/../basic/header.php" ?>

<div class="d-flex align-items-center">
   <h3 class="mr-2">Alle Posts von <?php echo $bloggername ?></h3>
   <small> (Follower: <?php echo $follower ?>)</small>
</div>

<?php if (empty($allPostsFromBlogger)) : ?>
   <h5><?php echo $bloggername ?> hat noch keine Posts erstellt</h5>
<?php else : ?>
   <?php $counter = 0; ?>
   <div class="d-flex flex-wrap post-gallery justify-content-center">
      <?php foreach ($allPostsFromBlogger as $post) : ?>
         <li class="post-gallery-item m-1">
            <?php if (in_array($post->id, $unreadPostIDsArray)) : ?>
               <div class="newPostTag position-absolute"> Neuer Post </div>
            <?php endif ?>
            <?php if (in_array($post->id, $modifiedPostIDsArray)) : ?>
               <div class="modifiedPostTag position-absolute"> Update </div>
            <?php endif ?>
            <div class="position-relative overflow-hidden">
               <div class="curtain-left position-absolute"></div>
               <div class="curtain-right position-absolute"></div>
               <div class="post-img-cont">
                  <img src="https://source.unsplash.com/800x60<?php echo $counter ?>" alt="">
               </div>
               <div class="post-gallery-item-title position-absolute">
                  <p class="postTitle m-0"><?php echo $post->title ?></p>
               </div>
               <a class="position-absolute w-100 h-100" href="post?postID=<?php echo $post->id ?>"></a>
            </div>
         </li>
         <?php $counter++ ?>
      <?php endforeach ?>
   </div>
<?php endif ?>

<?php require_once __DIR__ . "/../basic/footer.php" ?>