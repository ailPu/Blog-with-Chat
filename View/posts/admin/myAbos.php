<?php require_once __DIR__ . "/../../basic/header.php" ?>

<div class="container">

   <div class="d-flex align-items-center">
      <h3>Deine Abos</h3>
      <form id="search" class="d-flex ml-auto bg-info rounded" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
         <div class="d-flex align-items-center">
            <label class="my-0 mr-2" for="search">Blogger suchen:</label>
            <input type="text" name="search" id="search">
         </div>
         <input type="hidden" name="_token" value="<?= $_SESSION['_token'] ?>">
         <button class="btn" type="submit">Suchen</button>
      </form>
   </div>

   <?php if (empty($allAbosFromUser)) : ?>
      <p>Du hast noch keine Abos</p>
   <?php endif ?>
   <ul class="d-flex flex-column my-2">
      <?php foreach ($allAbosFromUser as $abo) : ?>
         <li class="d-flex align-items-center position-relative mb-2 pl-1">
            <a href="postsFrom?bloggerID=<?php echo $abo->bloggerID ?>"><?php echo $allBloggernames[$abo->bloggerID] ?></a>
            <a class="ml-auto btn btn-warning" href="unsubscribe?bloggerID=<?php echo $abo->bloggerID ?>">Abo stornieren</a>
            <?php if ($unreadPostsArray[$abo->bloggerID] != 0) echo
               "<div class=\"unreadPost-myAbos d-flex position-absolute align-items-center p-0 justify-content-end\">
               <span>{$unreadPostsArray[$abo->bloggerID]}</span>
               <i class=\"fas fa-envelope text-warning ml-1\"></i>
            </div>"
            ?>
         </li>

      <?php endforeach ?>
   </ul>
</div>




<?php require_once __DIR__ . "/../../basic/footer.php" ?>