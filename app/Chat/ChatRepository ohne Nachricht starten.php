<?php

namespace app\Chat;

class ChatRepository
{
   public $pdo;

   public function __construct(\PDO $pdo)
   {
      $this->pdo = $pdo;
   }

   public function all1on1Conv($username_ID)
   {

      $sql = "SELECT * FROM chat WHERE
      (chatStarter_ID = :username_ID)
      OR (chatPartner_ID = :username_ID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":username_ID" => $username_ID
      ]);
      $conv = $stmt->fetchAll(\PDO::FETCH_CLASS, "app\\Chat\\ChatModel");
      return $conv;
   }

   public function find1On1Conv($chatID)
   {
      $sql = "SELECT * FROM chatmessages
      WHERE chatID = :chatID ORDER BY timestamp desc";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":chatID" => $chatID,
      ]);
      $chatHistory = $stmt->fetchAll(\PDO::FETCH_CLASS, "app\\Chat\\ChatModel");
      return $chatHistory;
   }

   public function add1On1Conv($chatStarter, $chatStarter_ID, $chatPartner, $chatPartner_ID)
   {
      $sql = "INSERT INTO chat (chatStarter, chatStarter_ID, chatPartner, chatPartner_ID) VALUES (:chatStarter, :chatStarter_ID, :chatPartner, :chatPartner_ID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":chatStarter" => $chatStarter,
         ":chatStarter_ID" => $chatStarter_ID,
         ":chatPartner" => $chatPartner,
         ":chatPartner_ID" => $chatPartner_ID,
      ]);
   }

   public function conv1On1AddMessage($chatID, $chatStarter, $chatStarter_ID, $chatPartner, $chatPartner_ID, $msg)
   {
      $sql = "INSERT INTO chatmessages (chatID, chatStarter, chatStarter_ID, chatPartner, chatPartner_ID, msg) VALUES (:chatID, :chatStarter, :chatStarter_ID, :chatPartner, :chatPartner_ID, :msg)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":chatID" => $chatID,
         ":chatStarter" => $chatStarter,
         ":chatStarter_ID" => $chatStarter_ID,
         ":chatPartner" => $chatPartner,
         ":chatPartner_ID" => $chatPartner_ID,
         ":msg" => $msg,
      ]);
   }

   public function allGroupConv()
   {
      $username = $_SESSION['login'];
      $sql = "SELECT * FROM groupchat WHERE username = :username OR groupChatUser1 = :username OR groupChatUser2 = :username";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":username" => $username
      ]);
      $conv = $stmt->fetchAll(\PDO::FETCH_CLASS, "app\\Chat\\ChatGroupModel");
      return $conv;
   }

   public function findGroupConv($username, $groupChatUser1, $groupChatUser2)
   {
      $sql = "SELECT * FROM groupchat WHERE
      (username = :username OR groupChatUser1 = :username OR groupChatUser2 = :username)
      AND (username = :groupChatUser1 OR groupChatUser1 = :groupChatUser1 OR groupChatUser2 = :groupChatUser1)
      AND (username = :groupChatUser2 OR groupChatUser1 = :groupChatUser2 OR groupChatUser2 = :groupChatUser2) ";

      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":username" => $username,
         ":groupChatUser1" => $groupChatUser1,
         ":groupChatUser2" => $groupChatUser2,
      ]);
      $chatHistory = $stmt->fetchAll(\PDO::FETCH_CLASS, "app\\Chat\\ChatGroupModel");
      return $chatHistory;
   }

   public function addGroupConv($username, $groupChatUser1, $groupChatUser2, $msg)
   {
      $sql = "INSERT INTO groupchat (username, groupChatUser1, groupChatUser2, msg) VALUES (:username, :groupChatUser1,:groupChatUser2, :msg)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":username" => $username,
         ":groupChatUser1" => $groupChatUser1,
         ":groupChatUser2" => $groupChatUser2,
         ":msg" => $msg,
      ]);
   }

   public function getChatID($chatStarter_ID, $chatPartner_ID)
   {
      $sql = "SELECT id from chat WHERE
      (chatStarter_ID = :chatStarter_ID and chatPartner_ID = :chatPartner_ID) OR
      (chatStarter_ID = :chatPartner_ID and chatPartner_ID = :chatStarter_ID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         "chatStarter_ID" => $chatStarter_ID,
         "chatPartner_ID" => $chatPartner_ID,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Chat\\ChatModel');
      $chatID = $stmt->fetch(\PDO::FETCH_CLASS);
      return $chatID;
   }

   public function findChatPartner($chatID, $username_ID)
   {
      $sql = "SELECT * from chat WHERE
      id = :id AND
      (chatStarter_ID <> :username_ID OR chatPartner_ID <> :username_ID)";
      $stmt = $this->pdo->prepare($sql);;
      $stmt->execute([
         ":id" => $chatID,
         ":username_ID" => $username_ID,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Chat\\ChatModel');
      $chatPartner = $stmt->fetch(\PDO::FETCH_CLASS);
      return $chatPartner;
   }
}
