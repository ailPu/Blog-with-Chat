<?php

namespace app\Core;

use app\Chat\ChatRepository;
use app\Controller\PostsController;
use app\Controller\LoginController;
use app\Controller\DashboardController;
use app\Post\PostsRepository;
use app\Comment\CommentsRepository;
use app\Login\UserAccountsRepository;
use app\Login\LoginService;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use app\Abo\AboRepository;

const DEBUG = true;

class Container
{
   public $instances = [];
   public $receipts = [];

   public function __construct()
   {
      $this->receipts = [
         "postsController" => function () {
            return new PostsController($this->make("postsRepository"), $this->make("gump"), $this->make("commentsRepository"), $this->make("chatRepository"), $this->make("aboRepository"), $this->make("userAccountsRepository"));
         },
         "postsRepository" => function () {
            return new PostsRepository($this->make("pdo"));
         },
         "commentsRepository" => function () {
            return new CommentsRepository($this->make("pdo"));
         },
         "loginController" => function () {
            return new LoginController($this->make("loginService"), $this->make("gump"), $this->make("userAccountsRepository"));
         },
         "loginService" => function () {
            return new LoginService($this->make("userAccountsRepository"));
         },
         "dashboardController" => function () {
            return new DashboardController($this->make("postsRepository"), $this->make("commentsRepository"), $this->make("userAccountsRepository"), $this->make("phpMailer"), $this->make("phpMailerException"), $this->make("phpMailerSMTP"), $this->make("chatRepository"), $this->make("aboRepository"));
         },
         "userAccountsRepository" => function () {
            return new userAccountsRepository($this->make("pdo"));
         },
         "chatRepository" => function () {
            return new ChatRepository($this->make("pdo"));
         },
         "aboRepository" => function () {
            return new AboRepository($this->make("pdo"));
         },
         "phpMailer" => function () {
            return new PHPMailer();
         },
         "phpMailerSMTP" => function () {
            return new SMTP();
         },
         "phpMailerException" => function () {
            return new Exception();
         },
         "gump" => function () {
            return new \GUMP('de');
         },
         "pdo" => function () {
            try {
               $this->mysql = new \PDO(
                  'mysql:host=localhost;dbname=_eigenesFramework;charset=utf8',
                  'root',
                  '',
               );
            } catch (\PDOException $e) {
               if (DEBUG == true) {
                  echo $e->getMessage() . '. Fehler in Zeile: ' . $e->getLine();
               } else {
                  echo 'Fehler bei der Datenbankverbindung';
               }
            }
            return $this->mysql;
         }
      ];
   }

   public function make($name)
   {
      if (!empty($this->instances[$name])) {
         return $this->instances[$name];
      }
      if (!empty($this->receipts[$name])) {
         $this->instances[$name] = $this->receipts[$name](); //! GANZ WICHTIG HIER DIE RUNDEN KLAMMERN AM ENDE
      }
      return $this->instances[$name];
   }
}
