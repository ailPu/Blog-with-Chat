<?php

namespace app\Chat;

use app\Core\AbstractModel;

class ChatMessagesModel extends AbstractModel
{
   public $id;
   public $chatID;
   public $creatorID;
   public $msg;
   public $timestamp;
}
