<?php

namespace app\Core;

use ArrayAccess;

abstract class AbstractModel implements ArrayAccess
{
   public function offsetSet($offset, $value)
   {
      $this->offset = $value;
   }

   public function offsetExists($offset)
   {
      return $this->$offset;
   }

   public function offsetUnset($offset)
   {
      unset($this->$offset);
   }

   public function offsetGet($offset)
   {
      return $this->$offset;
   }
}
