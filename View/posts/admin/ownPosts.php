<?php require_once __DIR__ . "/../../basic/header.php" ?>
<?php
// header("refresh:60");
?>

<a href="addPost" class="btn btn-primary my-4">Neuen Post erstellen</a>

<h3>Meine Posts</h3>
<ul class="d-flex flex-column">
   <?php if (!is_null($allPostsFromUser)) : ?>
      <?php foreach ($allPostsFromUser as $post) : ?>
         <li class="border-bottom d-flex align-items-center position-relative pl-1 py-2">
            <?php if ($unreadCmtsArray[$post->id] != 0) echo
               "<div class=\"unreadCmt-ownPosts d-flex position-absolute align-items-center p-0 justify-content-end\">
               <span>{$unreadCmtsArray[$post->id]}</span>
               <i class=\"fas fa-envelope text-warning ml-1\"></i>
            </div>"
            ?>
            <a href="post?postID=<?php echo $post->id ?>"><?php echo $post->title ?></a>
            <div class="<?php if ($post->modifiedDate != NULL) echo "editPost" ?> ml-auto d-flex flex-column align-items-end px-1 position-relative">
               <?php if ($post->modifiedDate != NULL) : ?>
                  <div>
                     <div><strong class="text-warning">EDITIERT</strong> am <?php echo $post->modifiedDate ?></div>
                  </div>
               <?php endif ?>
               <div>erstellt am <?php echo ($post->modifiedDate == NULL) ? $post->creationDate : $post->oldCreationDate ?></div>
            </div>
            <a href="editPost?postID=<?php echo $post->id ?>" class="btn btn-warning ml-3">Post bearbeiten</a>
         </li>
      <?php endforeach ?>
   <?php endif ?>
</ul>

<?php require_once __DIR__ . "/../../basic/footer.php" ?>