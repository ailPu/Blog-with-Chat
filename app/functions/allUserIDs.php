<?php

function allUserIDs($allUsersObj, $allUserIDs)
{
   foreach ($allUsersObj as $user) {
      if (!in_array($user->id, $allUserIDs)) {
         $allUserIDs[] = $user->id;
      }
   }
   return $allUserIDs;
}
