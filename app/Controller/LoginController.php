<?php

namespace app\Controller;

use app\Core\AbstractController;
use app\Login\LoginService;
use app\Login\UserAccountsRepository;

class LoginController extends AbstractController
{

   public $loginService;
   public $postsRepository;

   public function __construct(LoginService $loginService, \GUMP $gump, UserAccountsRepository $userAccountsRepository)
   {
      $this->loginService = $loginService;
      $this->gump = $gump;
      $this->userAccountsRepository = $userAccountsRepository;
   }

   public function login()
   {
      $gumpErrors = [];
      $userExists = false;
      $userNotFound = false;

      //!Userabfrage 
      if (!empty($_POST['username']) && !empty($_POST['password'])) {
         $username = $_POST['username'];
         $password = $_POST['password'];

         //! Wenn das, was hier zurückkommt TRUE ist, dann gehe weiter 
         if ($this->loginService->loginAttempt($username, $password)) {
            if (!empty($_COOKIE["requestedLocation"])) {
               $requestedLocation = $_COOKIE['requestedLocation'];
               header("LOCATION: $requestedLocation");
            } else {
               header("LOCATION: dashboard");
               return;
            }
         } else {
            $userNotFound = true;
         }
      }

      //!Neuen User anlegen 
      if (!empty($_POST['usernameNew']) || !empty($_POST['passwordNew'])) {
         $usernameNew = trim($_POST['usernameNew']);
         $passwordNew = trim($_POST['passwordNew']);
         $passwordNew = password_hash($passwordNew, PASSWORD_BCRYPT);

         $this->gump::set_error_messages([
            'validate_regex' => 'Das Feld Passwort muss einen Großbuchstaben, Kleinbuchstaben, eine Ziffer zwischen 0-9 und ein Sonderzeichen enthalten',
         ]);
         $this->gump->validation_rules([
            'usernameNew' => 'required|max_len,20|min_len,6',
            'passwordNew' => 'required|max_len,20|min_len,6|regex,/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/',
         ]);

         //! Werden nicht angewendet. Warum auch immer 
         $this->gump->filter_rules([
            'usernameNew' => 'trim|sanitize_string',
            'passwordNew' => 'trim|sanitize_string'
         ]);

         $valid_data = $this->gump->run($_POST);
         if ($valid_data) {
            if ($this->userAccountsRepository->addUser($usernameNew, $passwordNew)) {
               $_SESSION['username'] = $usernameNew;
               $user = $this->userAccountsRepository->findByUsername($_SESSION['username']);
               $_SESSION['userID'] = $user->id;
               session_regenerate_id();
               header("LOCATION: dashboard");
            } else {
               $userExists = true;
            }
         }
         $gumpErrors = $this->gump->get_errors_array();
      }


      $this->render("/../../View/login.php", [
         "gumpErrors" => $gumpErrors,
         "userExists" => $userExists,
         "userNotFound" => $userNotFound
      ]);
   }

   public function logout()
   {
      $this->loginService->logout();
      header("LOCATION: login");
   }
}
