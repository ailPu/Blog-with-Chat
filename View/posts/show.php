<?php require_once __DIR__ . "/../basic/header.php" ?>

<div class="d-flex align-items-center mt-4">
   <h2>
      <?php echo $postObj->title ?>
   </h2>
   <div class="ml-auto d-flex flex-column align-items-end">
      <?php if ($postObj->modifiedDate != NULL) : ?>
         <div>
            <div>(<strong class="text-warning">UPDATE</strong> am <?php echo $postObj->modifiedDate ?>)</div>
         </div>
      <?php endif ?>
      <div>(<strong>geschrieben</strong> von <strong><?php echo $postCreatorObj->username ?></strong> am <?php echo $postObj->creationDate ?>)</div>
   </div>
</div>
<div class="post-content" class="card mt-2">
   <p class="p-2 mb-0">
      <?php echo nl2br($postObj->content) ?>
   </p>
</div>

<form action="<?php echo $_SERVER['PHP_SELF'] . '?postID=' . $postObj->id ?>" method="post">
   <div class="mt-3">
      <label for="comment">Kommentar hinzufügen</label>
   </div>
   <div>
      <textarea name="comment" id="comment" cols="50" rows="5" placeholder="Dein Kommentar"><?php if (!empty($_POST['comment'])) echo $_POST['comment'] ?></textarea>
   </div>
   <div><?php if (!empty($gumpErrors['comment'])) {
            echo $gumpErrors['comment'];
         } ?></div>
   <input type="hidden" name="_token" value="<?= $_SESSION['_token'] ?>">
   <button type="submit" class="btn btn-primary mt-3">Abschicken</button>
</form>


<?php if (!empty($unreadCmts)) : ?>
   <div class="d-flex flex-column align-items-center" id="newCmt">
      <div class="newCmt bg-success text-white mb-2">Du hast
         <?php if (count($unreadCmts) == 1) echo "einen neuen Kommentar";
         else echo "neue Kommentare" ?></div>
      <div><a href="#<?php echo "unreadCmt" ?>" class="btn btn-success">Zum neuen Kommentar</a></div>
   </div>
<?php endif ?>

<div class="comments">
   <h2 class="mt-3">Kommentare</h2>

   <?php foreach ($comments as $comment) : ?>
      <div id="<?php if (in_array($comment->id, $unreadCmts)) echo "unreadCmt";
               else echo $comment->id ?>" class="card 
         <?php if ($comment->creatorID == $postCreatorObj->id) {
            echo "bg-warning";
         } ?> 
         <?php if (in_array($comment->creatorID, $allCommentCreatorIDs)) {
            echo "bg-success";
         }
         ?>">
         <div class=" card-body d-flex align-items-center">
            <div class="col-7 align-self-center">
               <?php echo nl2br($comment->content) ?>
            </div>
            <small class="align-self-center ml-auto col-3">geschrieben von <strong>
                  <?php if ($comment->creatorID == $postCreatorObj->id) : ?>
                     <?php echo $postCreatorObj->username ?></strong> am <?php echo $comment->timestamp ?>
            <?php elseif ($comment->creatorID == 0) : ?>
               Gast</strong> am <?php echo $comment->timestamp ?>
            <?php else : ?>
               <?php if (array_key_exists($comment->creatorID, $allCommentCreatorNames)) : ?>
                  <?php echo $allCommentCreatorNames[$comment->creatorID] ?></strong> am <?php echo $comment->timestamp ?>
               <?php else : echo "gelöschtem User (UserID: " . $comment->creatorID . ")" ?></strong> am <?php echo $comment->timestamp ?>
               <?php endif ?>
            <?php endif ?>

            </small>
            <?php if (!is_null($unreadCmts)) : ?>
               <?php if (in_array($comment->id, $unreadCmts)) : ?>
                  <a href="setCommentRead?cmtID=<?php echo $comment->id ?>&postID=<?php echo $postObj->id ?>" class="btn btn-primary">Ungelesen</a>
                  <?php ?>
               <?php endif ?>
            <?php endif ?>
         </div>
      </div>
   <?php endforeach ?>
</div>




<?php require_once __DIR__ . "/../basic/footer.php" ?>