<?php

namespace app\Chat;

use app\Core\AbstractModel;

class ChatGroupModel extends AbstractModel
{
   public $id;
   public $username;
   public $groupChatUser1;
   public $groupChatUser2;
   public $msg;
   public $timestamp;
}
