<?php require_once __DIR__ . "/../basic/header.php" ?>
<?php
header("refresh:60");
?>

<!-- Damit jeder Eintrag ein eigenes Bild erhält  -->
<?php $counter = 0 ?>

<?php if (is_null($allPosts)) : ?>
   <h3>Es wurden noch keine Posts erstellt</h3>
<?php else : ?>
   <div class="d-flex flex-wrap post-gallery justify-content-center">
      <?php foreach ($allPosts as $post) : ?>
         <li class="post-gallery-item m-1">
            <?php if ((intval(strtotime($post->creationDate)) + (60 * 60 * 24 * 7)) > time()) : ?>
               <?php if ($post->modifiedDate == NULL) : ?>
                  <div class="newPostTag position-absolute">Neuer Post </div>
               <?php elseif ($post->modifiedDate != NULL) : ?>
                  <div class="modifiedPostTag position-absolute"> Update</div>
               <?php endif ?>
            <?php endif ?>
            <div class="position-relative overflow-hidden">
               <div class="curtain-left position-absolute"></div>
               <div class="curtain-right position-absolute"></div>
               <div class="post-img-cont">
                  <img src="https://source.unsplash.com/800x60<?php echo $counter ?>" alt="">
               </div>
               <div class="post-gallery-item-title position-absolute">
                  <p class="postTitle m-0"><?php echo $post->title ?></p>
               </div>
               <a class="position-absolute w-100 h-100" href="post?postID=<?php echo $post->id ?>"></a>
            </div>
         </li>
         <?php $counter++ ?>
      <?php endforeach ?>
   </div>
<?php endif ?>

<?php require_once __DIR__ . "/../basic/footer.php" ?>