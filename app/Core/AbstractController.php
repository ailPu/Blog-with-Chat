<?php

namespace app\Core;

abstract class AbstractController
{
   public function render($view, $params)
   {
      extract($params);
      require_once __DIR__ . $view;
   }
}
