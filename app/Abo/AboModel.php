<?php

namespace app\Abo;

use app\Core\AbstractModel;

class AboModel extends AbstractModel
{
   public $id;
   public $userID;
   public $bloggerID;
   public $timestamp;
}
