# Summary

Date : 2020-04-19 17:16:51

Directory d:\_Schnellzugriff Ordner\Dokumente\_Wifi\_eigenesFramework\app

Total : 31 files,  2837 codes, 154 comments, 405 blanks, all 3396 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| PHP | 31 | 2,837 | 154 | 405 | 3,396 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 31 | 2,837 | 154 | 405 | 3,396 |
| Abo | 2 | 125 | 0 | 19 | 144 |
| Chat | 7 | 603 | 3 | 88 | 694 |
| Comment | 2 | 197 | 4 | 26 | 227 |
| Controller | 5 | 1,272 | 87 | 175 | 1,534 |
| Core | 3 | 122 | 0 | 17 | 139 |
| Login | 4 | 169 | 6 | 29 | 204 |
| Post | 2 | 288 | 18 | 33 | 339 |
| Traits | 2 | 30 | 1 | 8 | 39 |
| functions | 4 | 31 | 35 | 10 | 76 |

[details](details.md)