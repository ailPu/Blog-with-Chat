<?php require_once __DIR__ . "/../../basic/header.php" ?>

<div class="d-flex align-items-center">
   <a href="chat" class="btn btn-primary my-3">Alle Unterhaltungen</a>
   <?php if (!in_array($chatPartnerID, $allUserIDs)) : ?>
   <?php elseif ($blockedByPartner == 1) : ?>
   <?php elseif (in_array($chatPartnerID, $blockedByUser)) : ?>
      <a href="unblockUser?chatID=<?php echo $chatID ?>&chatPartnerID=<?php echo $chatPartnerID ?>" class="btn btn-success ml-auto">Unterhaltung wieder aufnehmen</a>
   <?php elseif (!in_array($chatPartnerID, $blockedByUser)) : ?>
      <a class="btn btn-warning ml-auto mb-1" href="blockUser?chatID=<?php echo $chatID ?>">Unterhaltung beenden</a>
   <?php endif ?>
</div>

<div class="d-flex mt-1 justify-content-center">
   <h4 class="">Dein Chatverlauf mit
      <?php if (!in_array($chatPartnerID, $allUserIDs)) : echo "gelöschtem User (UserID: " . $chatPartnerID . ")" ?>
      <?php else : echo $chatPartner; ?>
      <?php endif ?>
   </h4>
</div>

<form action="<?php echo $_SERVER['PHP_SELF'] . '?chatID=' . $chatID ?>" method="post" class="text-center">
   <div>
      <label for="msg">Nachricht hinterlassen</label>
   </div>
   <div>
      <textarea class="rounded p-2" name="msg" id="msg" cols="50" rows="5" placeholder="Deine Nachricht"><?php if (!empty($_POST['msg'])) echo $_POST['msg'] ?></textarea>
   </div>
   <input type="hidden" name="_token" value="<?= $_SESSION['_token'] ?>">
   <?php if (!in_array($chatPartnerID, $allUserIDs)) : ?>
      <div class="chat-end bg-danger d-flex justify-content-center align-items-center mt-2">
         <p class="mb-0">
            <?php echo "Unterhaltung wurde beendet. Konto von $chatPartner existiert nicht mehr" ?>
         </p>
      </div>
   <?php elseif (in_array($chatPartnerID, $blockedByUser)) : ?>
      <div class="chat-end bg-danger d-flex justify-content-center align-items-center mt-2">
         <p class="mb-0">
            <?php echo "Du hast $chatPartner blockiert" ?>
         </p>
      </div>
   <?php elseif ($blockedByPartner == 1) : ?>
      <div class="chat-end bg-danger d-flex justify-content-center align-items-center mt-2">
         <p class="mb-0">
            <?php echo "$chatPartner hat dich blockiert" ?>
         </p>
      </div>
   <?php else : ?>
      <button type="submit" class="btn btn-outline-success mt-3">Nachricht abschicken</button>
   <?php endif ?>
</form>

<div class="d-flex flex-column mt-3 list-unstyled">
   <?php foreach ($chatHistory as $message) : ?>
      <div class="card border-0">
         <li class="rounded position-relative p-3 my-1 w-45
         <?php if ($message->creatorID == $_SESSION['userID']) : echo " bg-success text-white chatArrow-left"; ?>
         <?php else : echo " bg-info ml-auto chatArrow-right"; ?>
         <?php endif ?>">
            <div class="align-self-center">
               <?php echo nl2br($message->msg) ?>
            </div>
            <small class="">geschrieben von <strong>
                  <?php if ($message->creatorID == $_SESSION["userID"]) : echo $_SESSION['username']; ?>
                  <?php elseif ($chatPartner) : echo $chatPartner ?>
                  <?php else : echo "gelöschtem User (UserID: " . $message->creatorID . ")" ?>
                  <?php endif ?>
               </strong> am <?php echo $message->timestamp ?>
            </small>
         </li>
      </div>
   <?php endforeach ?>
</div>


<?php require_once __DIR__ . "/../../basic/footer.php" ?>