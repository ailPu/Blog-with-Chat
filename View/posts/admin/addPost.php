<?php require_once __DIR__ . "/../../basic/header.php" ?>

<div class="container">
   <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
      <h3>Neuer Beitrag</h3>
      <div class="form-group">
         <div>
            <label for="title">Titel:</label>
         </div>
         <input type="title" name="title" id="title" value="<?php if (!empty($_POST['title'])) echo $_POST['title'] ?>">
      </div>
      <div class="form-group">
         <div>
            <label for="content">Inhalt:</label>
         </div>
         <div>
            <textarea name="content" id="content" cols="30" rows="10"><?php if (!empty($_POST['content'])) echo $_POST['content'] ?></textarea>
         </div>
      </div>
      <input type="hidden" name="_token" value="<?= $_SESSION['_token'] ?>">
      <button type="submit" class="btn btn-primary">Neuen Beitrag erstellen</button>
   </form>
</div>

<?php require_once __DIR__ . "/../../basic/footer.php" ?>