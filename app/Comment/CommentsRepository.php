<?php

namespace app\Comment;

class CommentsRepository
{

   public function __construct(\PDO $pdo)
   {
      $this->pdo = $pdo;
   }

   public function insertForPost($content, $postID, $creatorID = 0, $postCreatorObj = NULL)
   {
      $sql = "INSERT INTO posts__cmts (content, postID, creatorID) VALUES (:content, :postID, :creatorID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":content" => $content,
         ":postID" => $postID,
         ":creatorID" => $creatorID,
      ]);

      if (!is_null($postCreatorObj)) {
         if ($postCreatorObj->id == $_SESSION['userID']) {
            $sql = "INSERT into posts__cmtsread (cmtID, userID) SELECT id,:userID from posts__cmts WHERE creatorID = :userID and id not in(SELECT cmtID from posts__cmtsread WHERE userID = :userID)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
               ":userID" => $creatorID,
            ]);
         }
      }
   }

   public function allForPost($postID)
   {
      $sql = "SELECT * FROM posts__cmts WHERE postID = :postID";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":postID" => $postID,
      ]);
      $allCommentsForPost = $stmt->fetchAll(\PDO::FETCH_CLASS, 'app\\Comment\\CommentModel');
      return $allCommentsForPost;
   }

   public function findCommentCreators($postID)
   {
      $sql = "SELECT id, username FROM users__ WHERE id in (SELECT creatorID FROM posts__cmts where postID = :postID and creatorID not in (SELECT creatorID FROM posts__ where id = :id))";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":postID" => $postID,
         ":id" => $postID,
      ]);
      $commentCreators = $stmt->fetchAll(\PDO::FETCH_CLASS, 'app\\Comment\\CommentModel');
      return $commentCreators;
   }

   public function countUnreadCmtsForPost($posts, $userID)
   {
      $unreadCmtsArray = [];
      foreach ($posts as $post) {
         $sql = "SELECT COUNT(*) as count FROM `posts__cmts` WHERE postID = :postID and id not in (SELECT cmtID FROM posts__cmtsread WHERE userID = :userID)";
         $stmt = $this->pdo->prepare($sql);
         $stmt->execute([
            ":postID" => $post->id,
            ":userID" => $userID,
         ]);
         $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Comment\\CommentModel');
         $unreadCmt = $stmt->fetch(\PDO::FETCH_CLASS);
         $unreadCmtsArray[$post->id] = $unreadCmt->count;
      }
      return $unreadCmtsArray;
   }

   public function countUnreadComments($userID)
   {
      $sql = "SELECT COUNT(*) as count FROM posts__cmts WHERE postID in (SELECT id from posts__ WHERE creatorID = :userID) and id not in (SELECT cmtID FROM posts__cmtsread WHERE userID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Comment\\CommentModel');
      $unreadCmtsArray = $stmt->fetch(\PDO::FETCH_CLASS);
      $unreadCmtsForNav = $unreadCmtsArray->count;
      return $unreadCmtsForNav;
   }

   public function unreadComments($postID, $userID)
   {
      $sql = "SELECT id FROM posts__cmts WHERE postID = :postID and id not in (SELECT cmtID FROM posts__cmtsread WHERE userID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":postID" => $postID,
         ":userID" => $userID,
      ]);
      $unreadCmts = $stmt->fetchAll(\PDO::FETCH_CLASS, 'app\\Comment\\CommentModel');
      return $unreadCmts;
   }

   //! braucht es gegenwärtig nicht 
   public function allCommentsForUser($postCreator)
   {
      $sql = "SELECT * FROM posts__cmts WHERE postCreator = :postCreator";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":postCreator" => $postCreator
      ]);
      $allCommentsForUser = $stmt->fetchAll(\PDO::FETCH_CLASS, 'app\\Comment\\CommentModel');
      return $allCommentsForUser;
   }

   public function deleteForPost($id)
   {
      $sql = "DELETE FROM posts__cmts WHERE id = :id";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":id" => $id
      ]);

      $sql = "DELETE from posts__cmtsread WHERE cmtID not in (SELECT id from posts__cmts)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();
   }

   // 
   public function deleteAllForPost($postID)
   {
      echo $postID;
      $sql = "DELETE FROM posts__cmts WHERE postID = :postID";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":postID" => $postID,
      ]);

      $sql = "DELETE from posts__cmtsread WHERE cmtID not in (SELECT id from posts__cmts)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();
   }

   //! ACHTUNG hier werden ALLE Kommentare gelöscht, also auch die bei anderen Posts, anstatt "geschrieben von gelöschtenm User". Kann gewünscht sein... Dann müsste man halt auch dasselbe bei den Chats machen...
   public function deleteAllCmtsFromOwnPosts($userID)
   {
      $sql = "DELETE FROM posts__cmts WHERE postID in (SELECT id from posts__ WHERE creatorID = :creatorID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":creatorID" => $userID,
      ]);
   }

   public function deleteAllReadCmts()
   {
      $sql = "DELETE FROM posts__cmtsread WHERE cmtID not in (SELECT id from posts__cmts)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();
   }

   public function getPostId($id)
   {
      $sql = "SELECT postID FROM posts__cmts WHERE id = :id";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":id" => $id
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Comment\\CommentModel');
      if ($comment = $stmt->fetch(\PDO::FETCH_CLASS)) {
         $postID = $comment->postID;
         return $postID;
      } else {
         return false;
      }
   }

   public function setCmtRead($commentID, $userID)
   {
      $sql = "INSERT INTO posts__cmtsread (cmtID, userID) VALUES (:cmtID, :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":cmtID" => $commentID,
         ":userID" => $userID,
      ]);
   }

   public function checkCmtRead($commentID, $userID)
   {
      $sql = "SELECT EXISTS (SELECT * FROM posts__cmtsread WHERE cmtID = :cmtID AND userID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":cmtID" => $commentID,
         ":userID" => $userID,
      ]);
      $cmtRead = $stmt->fetch(\PDO::FETCH_NUM);
      if ($cmtRead) {
         $cmtRead = $cmtRead[0];
      }
      return $cmtRead;
   }

   public function checkCommentExistsForPost($cmtID, $postID)
   {
      $sql = "SELECT EXISTS (SELECT * FROM posts__cmts WHERE postID = :postID AND id = :id)";
      // $sql = "SELECT id FROM posts__cmts WHERE id = :id and id in (SELECT id FROM posts__cmts WHERE postID = :postID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":id" => $cmtID,
         ":postID" => $postID,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_NUM);
      $cmtCheck = $stmt->fetch();
      $cmtCheck = $cmtCheck[0];
      return $cmtCheck;
   }
}
