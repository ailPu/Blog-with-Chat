<?php require_once __DIR__ . "/../../basic/header.php" ?>
<?php header("refresh:60"); ?>


<a href="chat" class="btn btn-primary my-3">Alle Unterhaltungen</a>
<?php if (!empty($chatsWithEveryUser)) : ?>
   <h4>Du chattest bereits mit allen Usern</h4>

<?php else : ?>
   <h4>Starte eine neue Unterhaltung</h4>
   <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
      <?php
      if (count($allUsersExceptLoggedUser) == 0) : ?>
         <div class="form-group">
            <p>Es gibt noch keine anderen User zum Chatten</p>
         </div>
      <?php else : ?>
         <label for="recipientUsername"></label>
         <div class="form-group">
            <select name="recipientUsername" id="recipientUsername">
               <option value="">User auswählen</option>
               <?php foreach ($allUsersExceptLoggedUser as $user) : ?>
                  <?php if (!in_array($user->id, $allChatPartnerIDs)) : ?>
                     <option value="<?php echo $user->username ?>"><?php echo $user->username ?> </option>
                  <?php endif ?>
               <?php endforeach ?>
            </select>
         </div>
         <button type="submit" class="btn btn-primary">Chat starten</button>
         <input type="hidden" name="_token" value="<?= $_SESSION['_token'] ?>">
      <?php endif ?>
   </form>

<?php endif ?>

<?php require_once __DIR__ . "/../../basic/footer.php" ?>