<?php require_once __DIR__ . "/../../basic/header.php" ?>

<a href="dashboard" class="btn btn-primary mt-4">Zurück zum Dashboard</a>

<div class="d-flex align-items-center mt-4">
   <h2 class="">Post bearbeiten</h2>
   <a href="deletePost?postID=<?php echo $post->id ?>" class="btn btn-danger ml-auto">Post löschen</a>
</div>

<form action=" <?php echo $_SERVER['PHP_SELF'] . '?postID=' . $post->id ?>" method="post">
   <div>
      <label for="title">Titel Bearbeiten: </label>
      <input type="text" name="title" id="title" value="<?php echo $post->title ?>">
   </div>
   <div>
      <label for="content">Inhalt Bearbeiten: </label>
   </div>
   <div>
      <textarea name="content" id="content" cols="50" rows="5"><?php echo $post->content ?></textarea>
   </div>
   <input type="hidden" name="_token" value="<?= $_SESSION['_token'] ?>">
   <button type=" submit" class="btn btn-success mt-3">Post ändern</button>
</form>

<div class="mt-4">
   <h2 class="my-3">Kommentare</h2>

   <?php foreach ($allCommentsForPost as $comment) : ?>
      <div id="<?php echo $comment->id ?>" class="card col-12
      <?php if ($comment->creatorID == $post->creatorID) {
         echo "bg-warning";
      } ?> 
      <?php if (in_array($comment->creatorID, $allCommentCreatorIDs)) {
         echo "bg-success";
      }
      ?>">
         <div class="card-body d-flex align-items-center">
            <div class="align-self-center">
               <?php echo nl2br($comment->content) ?>
            </div>
            <small class="align-self-center ml-auto col-4">geschrieben von <strong>
                  <?php if ($comment->creatorID == $_SESSION['userID']) : ?>
                     <?php echo $_SESSION['username'] ?></strong> am <?php echo $comment->timestamp ?>
            <?php elseif ($comment->creatorID == 0) : ?>
               Gast</strong> am <?php echo $comment->timestamp ?>
            <?php else : ?>
               <?php if (array_key_exists($comment->creatorID, $allCommentCreatorNames)) : ?>
                  <?php echo $allCommentCreatorNames[$comment->creatorID] ?></strong> am <?php echo $comment->timestamp ?>
               <?php else : echo "gelöschtem User (UserID: " . $comment->creatorID . ")" ?></strong> am <?php echo $comment->timestamp ?>
               <?php endif ?>
            <?php endif ?>
            </small>
            <a href="deleteComment?commentID=<?php echo $comment->id ?>&postID=<?php echo $post->id ?>" class="btn btn-danger">Kommentar löschen</a>
         </div>
      </div>
   <?php endforeach ?>
</div>



<?php require_once __DIR__ . "/../../basic/footer.php" ?>