<?php

namespace app\Comment;

use app\Core\AbstractModel;

class CommentModel extends AbstractModel
{
   public $id;
   public $content;
   public $postID;
   public $creatorID;
   public $timestamp;
}
