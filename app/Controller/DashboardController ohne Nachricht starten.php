<?php

namespace app\Controller;

use app\Chat\ChatRepository;
use app\Post\PostsRepository;
use app\Comment\CommentsRepository;
use app\Login\LoginService;
use app\Core\AbstractController;
use app\Login\UserAccountsRepository;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

class DashboardController extends AbstractController
{
   public function __construct(PostsRepository $postsRepository, CommentsRepository $commentsRepository, LoginService $loginService, UserAccountsRepository $userAccountsRepository, PHPMailer $phpMailer, Exception $phpMailerException, SMTP $phpMailerSMTP, ChatRepository $chatRepository)
   {
      $this->postsRepository = $postsRepository;
      $this->commentsRepository = $commentsRepository;
      $this->loginService = $loginService;
      $this->userAccountsRepository = $userAccountsRepository;
      $this->phpMailer = $phpMailer;
      $this->phpMailerException = $phpMailerException;
      $this->phpMailerSMTP = $phpMailerSMTP;
      $this->chatRepository = $chatRepository;
   }

   public function dashboard()
   {
      $this->loginService->checkIfLoggedIn();
      // $user = $this->userAccountsRepository->getUser($)
      //! ist aber eine ziemlich unsichere Methode, vorallem wird username nicht immer in der Session gespeichert werden oder nicht? 
      $username = $_SESSION['login'];
      $user = $this->userAccountsRepository->findByUsername($username);
      $posts = $this->postsRepository->findPostsFromUser($username);
      $this->render("/../../View/dashboard.php", [
         "posts" => $posts,
         "user" => $user
      ]);
   }

   public function editPost()
   {
      $id = $_GET['id'];

      if (!empty($_POST['content']) && !empty($_POST['title'])) {
         $content = $_POST['content'];
         $title = $_POST['title'];
         $this->postsRepository->update($content, $title, $id);
      }

      $comments = $this->commentsRepository->allForPost($id);
      $post = $this->postsRepository->find($id);
      $this->render("/../../View/posts/admin/editPost.php", [
         "post" => $post,
         "comments" => $comments
      ]);
   }

   public function deleteComment()
   {
      $id = $_GET['id'];
      $post_id = $this->commentsRepository->getPostId($id);
      $this->commentsRepository->deleteForPost($id);
      header("LOCATION: editPost?id=$post_id");
   }

   public function deletePost()
   {
      $id = $_GET['id'];
      $this->postsRepository->deletePost($id);
      //! Problem ist, dass ich hier dann ins commentsRepository mit der id vom Posteintrag fahre 
      $this->commentsRepository->deleteAllForPost($id);
      header("LOCATION: dashboard");
   }

   public function addPost()
   {
      if (!empty($_POST['title']) && !empty($_POST['content']) && $_POST['_token'] == $_SESSION['_token']) {
         $title = $_POST['title'];
         $content = $_POST['content'];
         $user = $_SESSION['login'];

         $this->postsRepository->add($user, $title, $content);
      }

      $_SESSION['_token'] = bin2hex(random_bytes(32));
      $this->render("/../../View/posts/admin/addPost.php", []);
   }

   public function deleteAccount()
   {
      $username = $_GET['username'];
      $this->userAccountsRepository->deleteUser($username);
      $this->postsRepository->deleteAllPosts($username);

      //! KOMMENTAR WIRD NICHT GELÖSCHT 
      $this->commentsRepository->deleteAllForPostWhenAccountRemoved($username);
      $this->loginService->logout();
      header("LOCATION: login");
   }

   public function sendEmail()
   {

      if (!empty($_POST['subject']) && !empty($_POST['msg']) && $_SESSION['_token'] == $_POST['_token']) {
         $subject = $_POST['subject'];
         $msg = $_POST['msg'];
         $name = $_SESSION['login'];


         try {
            $mail = new PHPMailer(true);
            $mail->SMTPOptions = array(
               'ssl' => array(
                  'verify_peer' => false,
                  'verify_peer_name' => false,
                  'allow_self_signed' => true
               )
            );
            //Server settings
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'mail.gmx.net';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'paulbaur@gmx.at';                     // SMTP username
            $mail->Password   = '25052ailpu!';                               // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            //Recipients
            $mail->setFrom("paulbaur@gmx.at", $name); // Name is optional
            $mail->addAddress("paulbaur@gmx.at");
            // $mail->addAddress('ellen@example.com');               
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

            // Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name     // Add a recipient

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML when true
            $mail->Subject = $subject;
            $mail->Body    = $msg;
            // Selbe Mail nur ohne HTML Tags
            $mail->AltBody    = strip_tags($msg);

            $mail->send();
         } catch (\Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
         }
      }
      $_SESSION['_token'] = bin2hex(random_bytes(32));
      $this->render("/../../View/posts/admin/sendEmail.php", []);
   }

   public function chat()
   {
      $username = $_SESSION['login'];
      $usernameObj = $this->userAccountsRepository->findByUsername($username);
      $username_ID = $usernameObj->id;
      $conv1On1 = $this->chatRepository->all1On1Conv($username_ID);
      $convGroup = $this->chatRepository->allGroupConv();

      $allUsersObj = $this->userAccountsRepository->allUsers();
      $allUserIDs = [];

      foreach ($allUsersObj as $user) {
         if (!in_array($user->id, $allUserIDs)) {
            $allUserIDs[] = $user->id;
         }
      }

      $this->render("/../../View/posts/admin/chatOverview.php", [
         "conv1On1" => $conv1On1,
         "convGroup" => $convGroup,
         "allUserIDs" => $allUserIDs,
      ]);
   }

   public function conversations1On1()
   {
      //! Hole Namen des Unterhaltungspartners 
      $username = $_SESSION['login'];
      $usernameObj = $this->userAccountsRepository->findByUsername($username);
      $username_ID = $usernameObj->id;

      $chatID = $_GET['chatID'];

      //! Später evtl besser in findChatPartners, mit S am Ende, also alle 
      $chatPartnerObj = $this->chatRepository->findChatPartner($chatID, $username_ID);

      $chatPartner = Null;
      //! Code kaputt, weil jetzt ein array kommt, weil fetchAll in ChatRepository. Nicht mehr fetch 
      $chatPartner = $chatPartnerObj->chatPartner;
      $chatPartner_ID = $chatPartnerObj->chatPartner_ID;

      if ($chatPartnerObj->chatPartner == $username) {
         $chatPartner = $chatPartnerObj->chatStarter;
         $chatPartner_ID = $chatPartnerObj->chatStarter_ID;
      };

      $allUsersObj = $this->userAccountsRepository->allUsers();
      $allUserIDs = [];

      foreach ($allUsersObj as $user) {
         if (!in_array($user->id, $allUserIDs)) {
            $allUserIDs[] = $user->id;
         }
      }

      if (!empty($_POST['msg']) && $_SESSION['_token'] == $_POST['_token']) {
         $msg = $_POST['msg'];
         $msg = htmlentities($msg, ENT_QUOTES, "utf-8");
         $msg = strip_tags($msg);
         $this->chatRepository->conv1On1addMessage($chatID, $username, $username_ID, $chatPartner, $chatPartner_ID, $msg);
      }
      $chatHistory = $this->chatRepository->find1On1Conv($chatID);
      $_SESSION['_token'] = bin2hex(random_bytes(32));
      $this->render("/../../View/posts/admin/conversation1On1.php", [
         "chatPartner" => $chatPartner,
         "chatPartner_ID" => $chatPartner_ID,
         "chatHistory" => $chatHistory,
         "allUserIDs" => $allUserIDs,
         "chatID" => $chatID
      ]);
   }

   public function new1On1Conversation()
   {
      $username = $_SESSION['login'];
      $usernameObj = $this->userAccountsRepository->findByUsername($username);
      $username_ID = $usernameObj->id;

      $allUsers = $this->userAccountsRepository->allUsersExceptLoggedUser($username);

      if (!empty($_POST['recipientUsername']) && $_SESSION['_token'] == $_POST['_token']) {
         $chatPartner = $_POST['recipientUsername'];
         $chatPartnerObj = $this->userAccountsRepository->findByUsername($chatPartner);
         $chatPartner_ID = $chatPartnerObj->id;

         //! Überprüfe ob dieser User in der CHATTABELLE schon vorhanden ist.
         $allChatUsersObj = $this->chatRepository->all1On1Conv($username_ID);
         $allChatUserIDs = [];
         foreach ($allChatUsersObj as $chatUser) {
            if (!in_array($chatUser->chatPartner_ID, $allChatUserIDs) || !in_array($chatUser->chatStarter_ID, $allChatUserIDs)) {
               $allChatUserIDs[] = $chatUser->chatPartner_ID;
               $allChatUserIDs[] = $chatUser->chatStarter_ID;
            }
         }

         if (!in_array($chatPartner_ID, $allChatUserIDs)) {
            $this->chatRepository->add1On1Conv($username, $username_ID, $chatPartner, $chatPartner_ID);
            //! Da bräuchte es wohl eigentlich nur mehr die chatPartner_ID oder?? Man prüft ja schon, ob der User noch nicht vorhanden ist. ODER?? 
            $chatIDObj = $this->chatRepository->getChatID($username_ID, $chatPartner_ID);
            $chatID = $chatIDObj->id;
            header("LOCATION: conversations1On1?chatID=$chatID");
         } else {
            //! hole dir chatID von der Unterhaltung mit dem ausgewählten USER
            $chatIDObj = $this->chatRepository->getChatID($username_ID, $chatPartner_ID);
            $chatID = $chatIDObj->id;
            header("LOCATION: conversations1On1?chatID=$chatID");
         }
      }
      $_SESSION['_token'] = bin2hex(random_bytes(32));
      $this->render("/../../View/posts/admin/new1On1Chat.php", [
         "allUsers" => $allUsers
      ]);
   }

   //! Mit 3 Leuten Chat öffnen
   public function newGroupConversation()
   {
      $errorSameUser = false;

      $username = $_SESSION['login'];
      $allUsers = $this->userAccountsRepository->allUsersExceptLoggedUser($username);
      //! Bedingungen aufbrechen, zwecks Übersichtlichkeit 

      //! Formular 
      if (!empty($_POST['groupChatUser1']) && !empty($_POST['groupChatUser2'])) {
         if ($_POST['groupChatUser1'] == $_POST['groupChatUser2']) {
            $errorSameUser = "Teilnehmer 1 und Teilnehmer 2 können nicht gleich sein";
         } else if (!empty($_POST['msg']) && $_SESSION['_token'] == $_POST['_token']) {
            $msg = $_POST['msg'];
            $msg = htmlentities($msg, ENT_QUOTES, "utf-8");
            $msg = strip_tags($msg);
            $username = $_SESSION['login'];
            $chatPartner1 = $_POST['groupChatUser1'];
            $chatPartner2 = $_POST['groupChatUser2'];
            $this->chatRepository->addGroupConv($username, $chatPartner1, $chatPartner2, $msg);
            header("LOCATION: conversationsGroup?groupChatUser1=$chatPartner1&groupChatUser2=$chatPartner2");
         }
      }

      $_SESSION['_token'] = bin2hex(random_bytes(32));

      $this->render("/../../View/posts/admin/newGroupChat.php", [
         "allUsers" => $allUsers,
         "errorSameUser" => $errorSameUser,
      ]);
   }

   public function conversationsGroup()
   {
      //! Hole Namen der Unterhaltungspartners 
      $username = $_SESSION['login'];
      $chatPartner1 = $_GET['groupChatUser1'];
      $chatPartner2 = $_GET['groupChatUser2'];
      $allUsersObj = $this->userAccountsRepository->allUsers();
      $allUsersArray = [];

      foreach ($allUsersObj as $user) {
         if (!in_array($user->username, $allUsersArray)) {
            $allUsersArray[] = $user->username;
         }
      }

      if (!empty($_POST['msg']) && $_SESSION['_token'] == $_POST['_token']) {
         $msg = $_POST['msg'];
         $this->chatRepository->addGroupConv($username, $chatPartner1, $chatPartner2, $msg);
      }

      $chatGroupHistory = $this->chatRepository->findGroupConv($username, $chatPartner1, $chatPartner2);
      $_SESSION['_token'] = bin2hex(random_bytes(32));
      $this->render("/../../View/posts/admin/conversationGroup.php", [
         "chatPartner1" => $chatPartner1,
         "chatPartner2" => $chatPartner2,
         "chatGroupHistory" => $chatGroupHistory,
         "allUsersArray" => $allUsersArray
      ]);
   }
}
