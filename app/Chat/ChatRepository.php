<?php

namespace app\Chat;

class ChatRepository
{
   public $pdo;

   public function __construct(\PDO $pdo)
   {
      $this->pdo = $pdo;
   }

   public function all1on1Conv($userID)
   {
      $sql = "SELECT * FROM chats__ WHERE
      (chatStarterID = :userID)
      OR (chatPartnerID = :userID) ORDER BY timestamp desc";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID
      ]);
      $conv = $stmt->fetchAll(\PDO::FETCH_CLASS, "app\\Chat\\ChatModel");
      return $conv;
   }

   public function countUnreadMessages($userID)
   {
      $sql = "SELECT COUNT(*) as count FROM chats__msgs where chatID in (SELECT id from chats__ WHERE (chatStarterID = :userID OR chatPartnerID = :userID)) and id not in (SELECT msgID from chats__msgsread WHERE userID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         "userID" => $userID,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, "app\\Chat\\ChatModel");
      $unreadMsgArray = $stmt->fetch(\PDO::FETCH_CLASS);
      $unreadMsgForNav = $unreadMsgArray->count;
      return $unreadMsgForNav;
   }

   public function countUnreadMessagesForConv($conversations, $userID)
   {
      $unreadMsgArray = [];
      foreach ($conversations as $conversation) {
         $sql = "SELECT COUNT(*) as count FROM chats__msgs where chatID=:chatID and id not in (SELECT msgID from chats__msgsread WHERE userID = :userID)";
         $stmt = $this->pdo->prepare($sql);
         $stmt->execute([
            ":chatID" => $conversation->id,
            ":userID" => $userID,
         ]);
         $stmt->setFetchMode(\PDO::FETCH_CLASS, "app\\Chat\\ChatModel");
         $unreadMsg = $stmt->fetch(\PDO::FETCH_CLASS);
         $unreadMsgArray[$conversation->id] = $unreadMsg->count;
      }
      return $unreadMsgArray;
   }

   public function allChatIDs($userID)
   {
      $sql = "SELECT id from chats__ WHERE (chatStarterID = :userID OR chatPartnerID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $chatIDs = $stmt->fetchAll(\PDO::FETCH_CLASS, "app\\Chat\\ChatModel");
      $chatIDsArray = [];
      foreach ($chatIDs as $chatID) {
         if (!in_array($chatID->id, $chatIDsArray)) {
            $chatIDsArray[] = $chatID->id;
         }
      }
      return $chatIDsArray;
   }

   public function allChatsFromUser($userID)
   {
      $sql = "SELECT * from chats__ WHERE (chatStarterID = :userID OR chatPartnerID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $allChatsFromUser = $stmt->fetchAll(\PDO::FETCH_CLASS, "app\\Chat\\ChatModel");
      return $allChatsFromUser;
   }

   public function find1On1Conv($chatID)
   {
      $sql = "SELECT * FROM chats__msgs
      WHERE chatID = :chatID ORDER BY timestamp desc";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":chatID" => $chatID,
      ]);
      $chatHistory = $stmt->fetchAll(\PDO::FETCH_CLASS, "app\\Chat\\ChatMessagesModel");
      return $chatHistory;
   }

   public function add1On1Conv($chatStarterID, $chatPartnerID)
   {
      $sql = "INSERT INTO chats__ (chatStarterID, chatPartnerID) VALUES (:chatStarterID, :chatPartnerID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":chatStarterID" => $chatStarterID,
         ":chatPartnerID" => $chatPartnerID,
      ]);
   }

   public function add1On1Msg($chatID, $userID, $msg)
   {
      $sql = "INSERT INTO chats__msgs (chatID,creatorID, msg) VALUES (:chatID, :creatorID, :msg)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":chatID" => $chatID,
         ":creatorID" => $userID,
         ":msg" => $msg,
      ]);

      $sql = "INSERT into chats__msgsread (msgID, userID) SELECT id,:userID from chats__msgs WHERE (creatorID = :creatorID and id not in (SELECT msgID from chats__msgsread WHERE userID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
         ":creatorID" => $userID,
      ]);
   }

   public function setMsgRead($chatID, $userID)
   {
      $sql = "INSERT into chats__msgsread (msgID, userID) SELECT id,:userID from chats__msgs WHERE (chatID = :chatID) and id not in (SELECT msgID from chats__msgsread WHERE userID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         "userID" => $userID,
         ":chatID" => $chatID,
      ]);
   }

   public function getChatID($chatStarterID, $chatPartnerID)
   {
      $sql = "SELECT * from chats__ WHERE
      (chatStarterID = :chatStarterID and chatPartnerID = :chatPartnerID) OR
      (chatStarterID = :chatPartnerID and chatPartnerID = :chatStarterID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         "chatStarterID" => $chatStarterID,
         "chatPartnerID" => $chatPartnerID,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Chat\\ChatModel');
      $chatID = $stmt->fetch(\PDO::FETCH_CLASS);
      return $chatID;
   }

   public function getChatByChatID($chatID, $userID)
   {
      //! Hier überprüfen, ob bei einer gepfuschten chatID, also form manipulation, der eingeloggte USER überhaupt existiert. Dann kann er gar nichts mehr ändern!!! 
      $sql = "SELECT * from chats__ WHERE
      (chatStarterID = :userID OR 
      chatPartnerID = :userID) AND
      id = :chatID";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":chatID" => $chatID,
         ":userID" => $userID
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Chat\\ChatModel');
      $chatObj = $stmt->fetch(\PDO::FETCH_CLASS);
      return $chatObj;
   }

   public function findChatPartner($chatID, $userID)
   {
      $sql = "SELECT * from chats__ WHERE
      id = :id AND
      (chatStarterID <> :userID OR chatPartnerID <> :userID)";
      $stmt = $this->pdo->prepare($sql);;
      $stmt->execute([
         ":id" => $chatID,
         ":userID" => $userID,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Chat\\ChatModel');
      $chatPartner = $stmt->fetch(\PDO::FETCH_CLASS);
      return $chatPartner;
   }

   public function allChatPartners($userID)
   {
      $allChatPartners = [];
      $sql = "SELECT id,username from users__ WHERE id in
      (SELECT chatStarterID from chats__ WHERE chatPartnerID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $chatPartnerPart1 = $stmt->fetchAll(\PDO::FETCH_CLASS, 'app\\Chat\\ChatModel');

      $sql = "SELECT id,username from users__ WHERE id in
      (SELECT chatPartnerID from chats__ WHERE chatStarterID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $chatPartnerPart2 = $stmt->fetchAll(\PDO::FETCH_CLASS, 'app\\Chat\\ChatModel');

      $mergedArray = array_merge($chatPartnerPart1, $chatPartnerPart2);
      foreach ($mergedArray as $array) {
         $allChatPartners[] = $array;
      }
      return $allChatPartners;
   }

   public function blockUser($blockedUserID, $userID)
   {
      $sql = "INSERT INTO chats__blocked (blockedUserID,userID) VALUES (:blockedUserID, :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
         ":blockedUserID" => $blockedUserID,
      ]);
   }

   public function unblockUser($blockedUserID, $userID)
   {
      $sql = "DELETE FROM chats__blocked WHERE blockedUserID = :blockedUserID AND userID = :userID";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":blockedUserID" => $blockedUserID,
         ":userID" => $userID,
      ]);
   }

   public function getBlockedUser($userID, $chatPartnerID)
   {
      $sql = "SELECT * from chats__blocked WHERE (userID = :userID AND blockedUserID = :chatPartnerID) OR (userID = :chatPartnerID AND blockedUserID = :userID) ";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
         ":chatPartnerID" => $chatPartnerID,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, 'app\\Chat\\ChatBlockedModel');
      $blockedUser = $stmt->fetch(\PDO::FETCH_CLASS);
      if ($blockedUser) {
         $blockedUser = $blockedUser->blockedUserID;
      }
      return $blockedUser;
   }

   //! Man könnte eigentlich auch einfach alles von der Tabell holen, wo man bei blocked oder userID steht und dann in einer Schleife zwei Arrays erstellen, wo wenn man an der Stelle "blockedUserID" steht, es in das blockedByPartners geht. Ansonsten ins "blockedByUser".. Egal für heute 
   public function blockedByUser($userID)
   {
      $blockedByUserArray = [];
      $sql = "SELECT * from chats__blocked WHERE userID = :userID";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $blockedUsers = $stmt->fetchAll(\PDO::FETCH_CLASS, 'app\\Chat\\ChatModel');
      foreach ($blockedUsers as $blockedUser) {
         $blockedByUserArray[] = $blockedUser->blockedUserID;
      }
      return $blockedByUserArray;
   }

   public function blockedByPartners($userID)
   {
      $blockedByPartnersArray = [];
      $sql = "SELECT * from chats__blocked WHERE blockedUserID = :userID";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $blockedByPartners = $stmt->fetchAll(\PDO::FETCH_CLASS, 'app\\Chat\\ChatModel');
      foreach ($blockedByPartners as $blockedByPartner) {
         $blockedByPartnersArray[] = $blockedByPartner->userID;
      }
      return $blockedByPartnersArray;
   }

   public function blockedByPartner($userID, $chatPartnerID)
   {
      $sql = "SELECT EXISTS (SELECT * FROM chats__blocked WHERE blockedUserID = :userID AND userID = :chatPartnerID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
         ":chatPartnerID" => $chatPartnerID,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_NUM);
      $blockedByPartner = $stmt->fetch();
      $blockedByPartner = $blockedByPartner[0];
      return $blockedByPartner;
   }

   public function deleteAllReadMsgs()
   {
      $sql = "DELETE FROM chats__msgsread WHERE userID not in (SELECT id from users__)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();
   }

   public function deleteChat()
   {
      $sql = "DELETE from chats__ WHERE(chatStarterID not in (SELECT id from users__) and chatPartnerID not in (SELECT id from users__))";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();

      $sql = "DELETE FROM chats__msgs WHERE chatID not in (SELECT id from chats__)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();

      $sql = "DELETE FROM chats__blocked WHERE(blockedUserID not in(SELECT id from users__) OR userID not in (SELECT id from users__))";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();
   }



   // ----------------------------- FEATURES AUFGELASSEN ------------------------------------------ \\ 






   public function allGroupConv()
   {
      $username = $_SESSION['username'];
      $sql = "SELECT * FROM groupchat WHERE username = :username OR groupChatUser1 = :username OR groupChatUser2 = :username";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":username" => $username
      ]);
      $conv = $stmt->fetchAll(\PDO::FETCH_CLASS, "app\\Chat\\ChatGroupModel");
      return $conv;
   }

   public function findGroupConv($username, $groupChatUser1, $groupChatUser2)
   {
      $sql = "SELECT * FROM groupchat WHERE
      (username = :username OR groupChatUser1 = :username OR groupChatUser2 = :username)
      AND (username = :groupChatUser1 OR groupChatUser1 = :groupChatUser1 OR groupChatUser2 = :groupChatUser1)
      AND (username = :groupChatUser2 OR groupChatUser1 = :groupChatUser2 OR groupChatUser2 = :groupChatUser2) ";

      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":username" => $username,
         ":groupChatUser1" => $groupChatUser1,
         ":groupChatUser2" => $groupChatUser2,
      ]);
      $chatHistory = $stmt->fetchAll(\PDO::FETCH_CLASS, "app\\Chat\\ChatGroupModel");
      return $chatHistory;
   }

   public function addGroupConv($username, $groupChatUser1, $groupChatUser2, $msg)
   {
      $sql = "INSERT INTO groupchat (username, groupChatUser1, groupChatUser2, msg) VALUES (:username, :groupChatUser1,:groupChatUser2, :msg)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":username" => $username,
         ":groupChatUser1" => $groupChatUser1,
         ":groupChatUser2" => $groupChatUser2,
         ":msg" => $msg,
      ]);
   }
}
