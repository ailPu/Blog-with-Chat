<?php require_once __DIR__ . "/basic/header.php" ?>

<h1>Bitte Einloggen</h1>

<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">

   <div class="form-group">
      <label for="username">Benutzername: </label>
      <input type="text" name="username" id="username" class="form-control col-md-4" value="<?php if (!empty($_POST['username'])) echo $_POST['username'] ?>">
   </div>
   <div class="form-group">
      <label for="password">Passwort: </label>
      <input type="password" name="password" id="password" class="form-control col-md-4">
   </div>
   <button type="submit" class="btn btn-primary">Einloggen</button>

</form>

<div class="text-danger">
   <?php if ($userNotFound) echo "Benutzername und Kennwort stimmen nicht überein" ?>
</div>
<br>

<h2>Noch nicht registriert?</h2>
<p>Neuen Benutzer erstellen</p>

<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">

   <div>
      <label for="usernameNew">Benutzername: </label>
      <input type="text" name="usernameNew" id="usernameNew" class="form-control col-md-4" value="<?php if (!empty($_POST['usernameNew'])) echo $_POST['usernameNew'] ?>">
      <?php if (!empty($gumpErrors['usernameNew'])) echo $gumpErrors['usernameNew'] ?>
   </div>
   <div>
      <label for=" passwordNew">Passwort: </label>
      <input type="password" name="passwordNew" id="passwordNew" class="form-control col-md-4" value="<?php if (!empty($_POST['passwordNew'])) echo $_POST['passwordNew'] ?>">
      <?php if (!empty($gumpErrors['passwordNew'])) echo $gumpErrors['passwordNew'] ?>
   </div>
   <button type="submit" class="btn btn-primary">Registrieren</button>
</form>

<div class="text-danger">
   <?php if ($userExists) echo "Dieser Username ist bereits vergeben" ?>
</div>

<?php require_once __DIR__ . "/basic/footer.php" ?>