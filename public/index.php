<?php require_once __DIR__ . "/../init.php";

session_start();

$path = null;
// Wenn ich in die index.php aufrufe, dann habe ich ja keine PATH_INFO
if (!isset($_SERVER["PATH_INFO"])) {
   $path = "/home";
} else {
   $path = $_SERVER['PATH_INFO'];
}
// //! Anhand der URL bzw. der PATH_INFO werden diese Parameter dem Container übergeben, der dann die entsprechenden Klassen und Methoden aufruft 

$routes = [
   "/home" => [
      "controller" => "postsController",
      "method" => "index",
   ],
   "/post" => [
      "controller" => "postsController",
      "method" => "show",
   ],
   "/login" => [
      "controller" => "loginController",
      "method" => "login",
   ],
   "/logout" => [
      "controller" => "loginController",
      "method" => "logout",
   ],
   "/dashboard" => [
      "controller" => "dashboardController",
      "method" => "dashboard",
   ],
   "/editPost" => [
      "controller" => "dashboardController",
      "method" => "editPost",
   ],
   "/setCommentRead" => [
      "controller" => "dashboardController",
      "method" => "setCommentRead",
   ],
   "/deleteComment" => [
      "controller" => "dashboardController",
      "method" => "deleteComment",
   ],
   "/deletePost" => [
      "controller" => "dashboardController",
      "method" => "deletePost",
   ],
   "/addPost" => [
      "controller" => "dashboardController",
      "method" => "addPost",
   ],
   "/ownPosts" => [
      "controller" => "dashboardController",
      "method" => "ownPosts",
   ],
   "/deleteAccount" => [
      "controller" => "dashboardController",
      "method" => "deleteAccount",
   ],
   "/stopDeleteAccount" => [
      "controller" => "dashboardController",
      "method" => "stopDeleteAccount",
   ],
   "/sendEmail" => [
      "controller" => "dashboardController",
      "method" => "sendEmail",
   ],
   "/chat" => [
      "controller" => "dashboardController",
      "method" => "chat",
   ],
   "/conv1On1" => [
      "controller" => "dashboardController",
      "method" => "conv1On1",
   ],
   "/new1On1Chat" => [
      "controller" => "dashboardController",
      "method" => "new1On1Conv",
   ],
   "/blockUserFromOverview" => [
      "controller" => "dashboardController",
      "method" => "blockUser",
   ],
   "/blockUser" => [
      "controller" => "dashboardController",
      "method" => "blockUser",
   ],
   "/unblockUser" => [
      "controller" => "dashboardController",
      "method" => "unblockUser",
   ],
   "/unblockUserFromOverview" => [
      "controller" => "dashboardController",
      "method" => "unblockUser",
   ],
   "/subscribe" => [
      "controller" => "dashboardController",
      "method" => "subscribe",
   ],
   "/subscribeFromSearch" => [
      "controller" => "dashboardController",
      "method" => "subscribe",
   ],
   "/unsubscribe" => [
      "controller" => "dashboardController",
      "method" => "unsubscribe",
   ],
   "/unsubscribeFromSearch" => [
      "controller" => "dashboardController",
      "method" => "unsubscribe",
   ],
   "/myAbos" => [
      "controller" => "dashboardController",
      "method" => "abos",
   ],
   "/postsFrom" => [
      "controller" => "postsController",
      "method" => "allPostsFromBlogger",
   ],
   "/searchResult" => [
      "controller" => "dashboardController",
      "method" => "searchResult",
   ],
   "/conversationsGroup" => [
      "controller" => "dashboardController",
      "method" => "conversationsGroup",
   ],
   "/newGroupChat" => [
      "controller" => "dashboardController",
      "method" => "newGroupConversation",
   ],
];

//! Routenüberprüfung
if (isset($routes[$path])) {
   $route = $routes[$path];
   $controller = $route["controller"];
   $method = $route["method"];
   $controller = $container->make($controller);
   $controller->$method();
} else {
   echo "Seite exisitiert nicht. Leite weiter zu Postübersicht";
   header('Refresh: 3; home');
}
