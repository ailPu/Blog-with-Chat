<?php

namespace app\Traits;

trait UnreadCounter
{
   public static function unreadCounter($chatRepository, $commentsRepository, $aboRepository, $postsRepository, $userID)
   {
      $unreadMsgCounter = $chatRepository->countUnreadMessages($userID);
      $_SESSION['unreadMsgCounterNav'] = $unreadMsgCounter;

      $unreadCmtsForUser = $commentsRepository->countUnreadComments($userID);
      $_SESSION['unreadCmtCounterNav'] = $unreadCmtsForUser;

      $unreadAbosForUser = $aboRepository->countUnreadAbos($userID);
      $_SESSION['unreadAboCounterNav'] = $unreadAbosForUser;
      //! Hier braucht es für die zweite Schleife zum Zählen alle Posts (die Frage ist, ob ich wirklich ALLE nehme oder vielleicht doch schon versuche, das ganze zu begrenzen auf NUR meine Abos... irgendwann würden ja milliarden Posts durchlaufen werden müssen, obwohl ich nur 50 abonniert habe....)
   }
}
