<?php require_once __DIR__ . "/basic/header.php" ?>
<?php
// header("refresh:60");
?>

<h1 class="pb-3">Willkommen in deinem Dashboard, <?php echo $_SESSION['username'] ?> </h1>
<h4 class="mb-3">Du hast <?php echo $follower ?> Follower</h4>


<div class="mb-4">
   <a href="addPost" class="btn btn-primary">Neuen Post erstellen</a>
   <a href="chat" class="btn btn-primary">Chat</a>
   <a href="sendEmail" class="btn btn-primary">Email an Support senden</a>
</div>

<h3>Meine Posts</h3>
<ul class="d-flex flex-column">
   <?php if (!is_null($allPostsFromUser)) : ?>
      <?php foreach ($allPostsFromUser as $post) : ?>
         <li class="border-bottom d-flex align-items-center position-relative pl-1 py-2">
            <?php if ($unreadCmtsArray[$post->id] != 0) echo
               "<div class=\"unreadCmt-ownPosts d-flex position-absolute align-items-center p-0 justify-content-end\">
               <span>{$unreadCmtsArray[$post->id]}</span>
               <i class=\"fas fa-envelope text-warning ml-1\"></i>
            </div>"
            ?>
            <a href="post?postID=<?php echo $post->id ?>"><?php echo $post->title ?></a>
            <div class="<?php if ($post->modifiedDate != NULL) echo "editPost" ?> ml-auto d-flex flex-column align-items-end px-1 position-relative">
               <?php if ($post->modifiedDate != NULL) : ?>
                  <div>
                     <div><strong class="text-warning">EDITIERT</strong> am <?php echo $post->modifiedDate ?></div>
                  </div>
               <?php endif ?>
               <div>erstellt am <?php echo ($post->modifiedDate == NULL) ? $post->creationDate : $post->oldCreationDate ?></div>
            </div>
            <a href="editPost?postID=<?php echo $post->id ?>" class="btn btn-warning ml-3">Post bearbeiten</a>
         </li>
      <?php endforeach ?>
   <?php endif ?>
</ul>

<?php if ($removeAccount == true) : ?>
   <div class="overlay position-absolute"></div>
   <div class="remove-account position-absolute border border-danger text-center">
      <h4>Konto wirklich löschen?</h4>
      <div class="d-flex">
         <a class="nav-link text-center btn btn-danger flex-1" href="deleteAccount?username=<?php echo $_SESSION['username'] ?>">JA<span class="sr-only"></span></a>
         <a class="nav-link text-center btn btn-success flex-1 ml-3" href="stopDeleteAccount?username=<?php echo $_SESSION['username'] ?>">NEIN<span class="sr-only"></span></a>
      </div>
   </div>

<?php endif ?>

<?php require_once __DIR__ . "/basic/footer.php" ?>