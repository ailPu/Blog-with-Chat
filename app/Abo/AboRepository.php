<?php

namespace app\Abo;

class AboRepository
{
   public function __construct(\PDO $pdo)
   {
      $this->pdo = $pdo;
   }

   public function subscribe($userID, $bloggerID)
   {
      $sql = "INSERT into abos__ (userID,bloggerID) VALUES (:userID, :bloggerID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
         ":bloggerID" => $bloggerID
      ]);

      $sql = "INSERT into abos__read (postID,userID) SELECT id,:userID from posts__ WHERE creatorID = :creatorID AND id not in (SELECT postID from abos__read WHERE userID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":creatorID" => $bloggerID,
         ":userID" => $userID,
      ]);
   }

   public function unsubscribe($userID, $bloggerID)
   {
      $sql = "DELETE FROM abos__ WHERE userID = :userID and bloggerID = :bloggerID";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
         ":bloggerID" => $bloggerID,
      ]);

      $sql = "DELETE FROM abos__read WHERE userID = :userID and postID in (SELECT id FROM posts__ WHERE creatorID = :creatorID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
         ":creatorID" => $bloggerID,
      ]);
   }

   public function allAbosFromUser($userID)
   {
      $sql = "SELECT * from abos__ WHERE userID = :userID";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $allAbos = $stmt->fetchAll(\PDO::FETCH_CLASS, "\\app\\Abo\\AboModel");
      return $allAbos;
   }

   public function countUnreadAbos($userID)
   {
      $sql = "SELECT COUNT(*) as count FROM posts__ where creatorID in
      (SELECT bloggerID from abos__ WHERE userID = :userID) and id not in 
      (SELECT postID from abos__read WHERE userID = :userID)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $stmt->setFetchMode(\PDO::FETCH_CLASS, "\\app\\Abo\\AboModel");
      $unreadAboArray = $stmt->fetch(\PDO::FETCH_CLASS);
      $unreadAboForNav = $unreadAboArray->count;

      $sql = "SELECT * FROM posts__ where creatorID in
      (SELECT bloggerID from abos__ WHERE userID = :userID) and modifiedDate IS NOT NULL";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $modifiedPosts = $stmt->fetchAll(\PDO::FETCH_CLASS, 'app\\Post\\PostModel');
      $modifiedTimestamps = [];
      if ($modifiedPosts) {
         foreach ($modifiedPosts as $modifiedPost) {
            $modifiedTimestamps[$modifiedPost->id] = $modifiedPost->modifiedDate;
         }
      }

      $sql = "SELECT * from abos__read WHERE userID = :userID";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $allReadPosts = $stmt->fetchAll(\PDO::FETCH_CLASS, 'app\\Post\\PostModel');
      $unreadModifiedPosts = 0;
      if ($allReadPosts) {
         foreach ($allReadPosts as $readPost) {
            if (array_key_exists($readPost->postID, $modifiedTimestamps) && $readPost->timestamp < $modifiedTimestamps[$readPost->postID]) {
               $unreadModifiedPosts++;
            }
         }
      }

      $unreadAboForNavToInt = intval($unreadAboForNav);
      $unreadAboModifiedAbo = $unreadAboForNavToInt + $unreadModifiedPosts;
      return $unreadAboModifiedAbo;
   }

   public function countFollowers($userID)
   {
      $sql = "SELECT COUNT(*) from abos__ WHERE bloggerID = :userID";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
         ":userID" => $userID,
      ]);
      $count = $stmt->fetch(\PDO::FETCH_NUM);
      $count = $count[0];
      return $count;
   }

   public function deleteAllAbos()
   {
      $sql = "DELETE FROM abos__ WHERE userID not in (SELECT id from users__) or bloggerID not in (SELECT id from users__)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();
   }

   public function deleteAllReadAbos()
   {
      $sql = "DELETE FROM abos__read WHERE userID not in (SELECT id from users__) or postID not in (SELECT id from posts__)";
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();
   }
}
